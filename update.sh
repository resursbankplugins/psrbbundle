#!/bin/bash -e
if [ ! -f "./composer.json" ]; then
  echo "Missing comoser.json. Please execute this script from the base directory."
  exit 1
fi

if [ -d "./modules" ]; then
  rm -rf "./modules"
fi

composer install
mkdir -p modules

packages="psrbcore psrbsimplified psrbordermanagement psrbpartpayment"

for package in $packages; do
  if [ ! -d "./vendor/resursbank/${package}" ]; then
    echo "Missing package ${package}"
    exit 1
  fi

  mv "./vendor/resursbank/${package}" ./modules/
done

if [ -d "./vendor/composer" ]; then
  rm -rf "./vendor/composer"
fi

if [ -f "./vendor/autoload.php" ]; then
  rm -rf "./vendor/autoload.php"
fi

if [ -f "./composer.lock" ]; then
  rm -rf "./composer.lock"
fi

mv "./vendor" "./modules/psrbcore/vendor"
