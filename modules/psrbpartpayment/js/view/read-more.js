/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

/**
 *
 * @param {RbcView.ReadMoreFnParams} data
 * @returns {RbcView.ReadMoreInstance}
 */
window.psrbpartpayment.View.ReadMore = function (data) {
    return window.psrbcore.View.ReadMore(data);
};
