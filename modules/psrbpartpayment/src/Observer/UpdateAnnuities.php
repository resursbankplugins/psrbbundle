<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\PartPayment\Observer;

use InvalidArgumentException;
use Resursbank\Core\Api\Credentials;
use Resursbank\Core\Entity\ResursbankPaymentMethod;
use Resursbank\Core\Logger\LoggerInterface;
use Resursbank\PartPayment\Config\Config;
use Resursbank\PartPayment\Helper\Method;
use Resursbank\PartPayment\Service\Annuity\Sync;

class UpdateAnnuities
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var Method
     */
    protected $methodHelper;

    /**
     * @var Sync
     */
    protected $syncService;

    /**
     * @var Credentials
     */
    protected $credentials;
    /**
     * @var Config
     */
    private $config;

    /**
     * @param Method $methodHelper
     * @param Sync $syncService
     * @param Credentials $credentials
     * @param LoggerInterface $logger
     * @param Config $config
     */
    public function __construct(
        Method $methodHelper,
        Sync $syncService,
        Credentials $credentials,
        LoggerInterface $logger,
        Config $config
    ) {
        $this->logger = $logger;
        $this->methodHelper = $methodHelper;
        $this->syncService = $syncService;
        $this->credentials = $credentials;
        $this->config = $config;
    }

    /**
     * @param array $params
     *
     * @return void
     *
     * @throws InvalidArgumentException
     */
    public function execute(array $params): void
    {
        if (!$this->config->getEnabled()) {
            return;
        }
        // Validate anonymous data in $params.
        $this->validateParams($params);

        $paymentMethods = $params['methods'];

        /** @var ResursbankPaymentMethod $method */
        foreach ($paymentMethods as $method) {
            try {
                if ($this->methodHelper->isEligible($method)) {
                    $this->syncService->deleteOldEntries(
                        $this->syncService->sync($method, $this->credentials->get()),
                        $method
                    );
                }
            } catch (\Exception $e) {
                $this->logger->error('Unable to update annuity for payment method ' . $method->getCode());
                $this->logger->exception($e);
            }
        }
    }

    /**
     * @param array $params
     *
     * @return void
     */
    private function validateParams(array $params): void
    {
        if (!isset($params['methods']) || !is_array($params['methods'])) {
            throw new InvalidArgumentException('No methods supplied');
        }
    }
}
