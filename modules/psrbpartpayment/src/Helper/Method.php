<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\PartPayment\Helper;

use Resursbank\Core\Entity\ResursbankPaymentMethod;
use function array_key_exists;
use function is_array;
use JsonException;

class Method
{
    /**
     * Check if a payment method is eligible to be utilised for part payments.
     *
     * @param ResursbankPaymentMethod $method
     * @return bool
     * @throws JsonException
     */
    public function isEligible(
        ResursbankPaymentMethod $method
    ): bool {
        $raw = json_decode(
            (string) $method->getRaw(),
            true,
            512,
            JSON_THROW_ON_ERROR
        );

        return (
            is_array($raw) &&
            array_key_exists('specificType', $raw) &&
            (
                $raw['specificType'] === 'PART_PAYMENT' ||
                $raw['specificType'] === 'REVOLVING_CREDIT'
            )
        );
    }
}
