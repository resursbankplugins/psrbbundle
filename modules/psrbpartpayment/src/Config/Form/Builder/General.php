<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\PartPayment\Config\Form\Builder;

use PrestaShop\PrestaShop\Core\Form\FormChoiceProviderInterface;
use Resursbank\PartPayment\Config\Form\ChoiceProvider\Annuity;
use Resursbank\PartPayment\Entity\ResursbankAnnuity;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use function is_int;

/**
 * Generates the form containing general order management settings.
 */
class General extends AbstractType
{
    /**
     * @var FormChoiceProviderInterface
     */
    protected $stateChoiceProvider;
    /**
     * @var FormChoiceProviderInterface
     */
    protected $methodChoiceProvider;
    /**
     * @var Annuity
     */
    protected $annuityChoiceProvider;

    /**
     * @param FormChoiceProviderInterface $stateChoiceProvider
     * @param FormChoiceProviderInterface $methodChoiceProvider
     * @param FormChoiceProviderInterface $annuityChoiceProvider
     */
    public function __construct(
        FormChoiceProviderInterface $stateChoiceProvider,
        FormChoiceProviderInterface $methodChoiceProvider,
        FormChoiceProviderInterface $annuityChoiceProvider
    ) {
        $this->stateChoiceProvider = $stateChoiceProvider;
        $this->methodChoiceProvider = $methodChoiceProvider;
        $this->annuityChoiceProvider = $annuityChoiceProvider;
    }

    /**
     * {@inheritdoc}
     *
     * @noinspection PhpMissingParentCallCommonInspection
     */
    public function buildForm(
        FormBuilderInterface $builder,
        array $options
    ): void {
        $builder
            ->add('enabled', ChoiceType::class, [
                'label' => 'Enabled',
                'choices' => $this->stateChoiceProvider->getChoices(),
                'required' => true,
            ])
            ->add('method', ChoiceType::class, [
                'label' => 'Method',
                'choices' => $this->methodChoiceProvider->getChoices(),
                'choice_value' => function ($method) {
                    if (is_int($method)) {
                        return $method;
                    }

                    return $method ? $method->getMethodId() : '';
                },
                'choice_label' => 'title',
                'placeholder' => 'Please select',
                'required' => false,
            ])
            ->add('annuity', ChoiceType::class, [
                'label' => 'Annuity',
                'choices' => $this->annuityChoiceProvider->getChoices(),
                'choice_value' => function ($annuity) {
                    if (is_int($annuity)) {
                        return $annuity;
                    }

                    return $annuity ? $annuity->getAnnuityId() : '';
                },
                'choice_label' => 'title',
                'choice_attr' => function (?ResursbankAnnuity $annuity) {
                    return $annuity ? ['method-id' => $annuity->getMethodId()] : [];
                },
                'placeholder' => 'Please select',
                'required' => false,
            ])
            ->add('threshold', TextType::class, [
                'label' => 'Threshold',
                'required' => false,
            ]);
    }

    /**
     * {@inheritdoc}
     *
     * @noinspection PhpMissingParentCallCommonInspection
     */
    public function getBlockPrefix(): string
    {
        return 'resursbank_partpayment_general';
    }
}
