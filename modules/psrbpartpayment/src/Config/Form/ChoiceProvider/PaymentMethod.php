<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\PartPayment\Config\Form\ChoiceProvider;

use Exception;
use PrestaShop\PrestaShop\Core\Form\FormChoiceProviderInterface;
use Resursbank\Core\Api\Credentials;
use Resursbank\Core\Logger\LoggerInterface;
use Resursbank\Core\Repository\ResursbankPaymentMethodRepository;
use Resursbank\PartPayment\Helper\Method;

/**
 * Provides payment methods for part payment.
 */
class PaymentMethod implements FormChoiceProviderInterface
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var Credentials
     */
    protected $credentials;

    /**
     * @var Method
     */
    protected $methodHelper;

    /**
     * @var ResursbankPaymentMethodRepository
     */
    protected $resursbankPaymentMethodRepository;

    /**
     * @param LoggerInterface $logger
     * @param ResursbankPaymentMethodRepository $resursbankPaymentMethodRepository
     * @param Credentials $credentials
     * @param Method $methodHelper
     */
    public function __construct(
        LoggerInterface $logger,
        ResursbankPaymentMethodRepository $resursbankPaymentMethodRepository,
        Credentials $credentials,
        Method $methodHelper
    ) {
        $this->logger = $logger;
        $this->credentials = $credentials;
        $this->methodHelper = $methodHelper;
        $this->resursbankPaymentMethodRepository = $resursbankPaymentMethodRepository;
    }

    /**
     * Provides a list of payment methods that are eligible for part payment.
     *
     * @return array
     */
    public function getChoices(): array
    {
        $result = [];

        try {
            $methods = $this->resursbankPaymentMethodRepository
                ->getList($this->credentials->get());

            foreach ($methods as $method) {
                if ($this->methodHelper->isEligible($method) && $method->getMethodId() !== null && $method->getTitle() !== null) {
                    $result[] = $method;
                }
            }
        } catch (Exception $e) {
            $this->logger->exception($e);
        }

        return $result;
    }
}
