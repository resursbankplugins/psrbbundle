<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{psrbpartpayment}prestashop>psrbpartpayment_6f36d3862f31995d464f58f0e204ccba'] = 'Uppdatering av state misslyckades.';
$_MODULE['<{psrbpartpayment}prestashop>psrbpartpayment_682d92f7f0f1cd5023a93744e56071ff'] = 'Uppdatering av metod-ID misslyckades.';
$_MODULE['<{psrbpartpayment}prestashop>psrbpartpayment_9d61eb956bec75618cff54abbef79a52'] = 'Uppdatering av ränte-ID misslyckades.';
$_MODULE['<{psrbpartpayment}prestashop>psrbpartpayment_38761b30e1aff227596a1d95e9cfa1b0'] = 'Uppdatering av tröskelvärde misslyckades.';
$_MODULE['<{psrbpartpayment}prestashop>psrbpartpayment_b4a34d3f58b039e7685c2e39b5413757'] = 'Uppdatering lyckades.';
$_MODULE['<{psrbpartpayment}prestashop>psrbpartpayment_f22a05ff99f0d836b9c75167ad82cc46'] = 'Resurs Bank Delbetalning';

