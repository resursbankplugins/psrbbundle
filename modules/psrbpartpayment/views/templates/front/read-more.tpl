<!--
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */
-->
{include 'module:psrbcore/views/templates/front/modals/read-more.tpl'}
<script>
    window.addEventListener('DOMContentLoaded', function () {
        window.psrbpartpayment.View.ReadMore({
            remodalId: '{$remodalId}',
            contentId: '{$contentId}',
            loaderId: '{$loaderId}',
            methodCode: '{$method}',
            price: {$price},
            url: window.psrbcore.getCostOfPurchaseUrl
        });
    });
</script>
