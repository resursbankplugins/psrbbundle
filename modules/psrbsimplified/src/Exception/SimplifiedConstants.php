<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Simplified\Exception;

use PrestaShopException;

/**
 * Collection with constants used by the Simplified module when throwing exceptions.
 * Each exception should have its own exception code so errors can be easier backtracked.
 * @since 1.0.0
 */
class SimplifiedConstants extends PrestaShopException
{
    /**
     * Exception: When there is no properly set cart.
     *
     * @var int
     * @since 1.0.0
     */
    public const INCONSISTENT_CART_EXCEPTION = 1000;

    /**
     * Exception: When getting data from the cart and a requested property does not exist.
     *
     * @var int
     * @since 1.0.0
     */
    public const NONEXISTENT_PROPERTY_EXCEPTION = 1001;

    /**
     * Exception: When we try to fetch a currency id, and it is not set.
     *
     * @var int
     * @since 1.0.0
     */
    public const NONEXISTENT_CURRENCY_EXCEPTION = 1002;

    /**
     * Exception: When we try to get a proper order id and the order id does not exist.
     *
     * @var int
     * @since 1.0.0
     */
    public const NONEXISTENT_ORDER_EXCEPTION = 1003;

    /**
     * Exception: When we request an order reference from a created order and the reference is not available.
     *
     * @var int
     * @since 1.0.0
     */
    public const NONEXISTENT_ORDER_REFERENCE_EXCEPTION = 1004;

    /**
     * Exception: When trying to fetch the id for the current module but the module id is not set.
     *
     * @var int
     * @since 1.0.0
     */
    public const MODULE_ID_MISSING_EXCEPTION = 1005;

    /**
     * Exception: When $cart->id_lang is not set.
     * @var int
     * @since 1.0.0
     */
    public const NONEXISTENT_CART_LANGUAGE_EXCEPTION = 1006;

    /**
     * Exception: When trying to retrieve order reference, but the reference does not exist.
     * This value is already taken and only marked in ths function.
     * @var int
     * @since 1.0.0
     */
    public const NONEXISTENT_ORDER_REFERENCE = 1007;

    /**
     * Missing order data exception.
     * @var int
     * @since 1.0.0
     */
    public const MISSING_ORDER_DATA_EXCEPTION = 1008;

    /**
     * Exception: When payment method id is not set in the simplified API (from getPaymentMethodId).
     * @var int
     * @since 1.0.0
     */
    public const MISSING_PAYMENT_METHOD_ID = 1009;

    /**
     * Exception: Currently used in failUrl processing if there is no defined cart id defined on the call.
     * @var int
     * @since 1.0.0
     */
    public const MISSING_CART_ID = 1010;

    /**
     * Exception: During cart rebuilding we also cancel the prior order. If there is no order id, you will se this code.
     * @var int
     * @since 1.0.0
     */
    public const MISSING_ORDER_ID = 1011;

    /**
     * @var int
     * @since 1.0.0
     */
    public const INVALID_PAYMENT_METHOD = 1012;

    /**
     * @var int
     * @since 1.0.0
     */
    public const MISSING_GOVERNMENT_ID = 1013;

    /**
     * @var int
     * @since 1.0.0
     */
    public const MISSING_PHONE_NUMBER = 1014;
}
