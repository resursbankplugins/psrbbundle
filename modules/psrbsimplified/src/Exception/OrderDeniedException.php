<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Simplified\Exception;

use PrestaShopException;

/**
 * Indicates requested order was not found.
 */
class OrderDeniedException extends PrestaShopException
{
}
