<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Simplified\Exception;

use PrestaShopException;

/**
 * Indicates a problem with the API service.
 * @since 1.0.0
 */
class ApiException extends PrestaShopException
{
}
