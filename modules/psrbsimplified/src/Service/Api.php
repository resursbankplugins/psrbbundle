<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Simplified\Service;

use Address;
use Cart;
use Context;
use function count;
use Exception;
use FrontController;
use function is_array;
use function is_object;
use JsonException;
use Order;
use PrestaShop\PrestaShop\Adapter\Entity\Customer;
use PrestaShop\PrestaShop\Adapter\Entity\Hook;
use PrestaShop\PrestaShop\Adapter\Entity\Tools;
use PrestaShop\PrestaShop\Core\Domain\Shop\ValueObject\ShopConstraint;
use PrestaShopException;
use Resursbank\Core\Api\Connection;
use Resursbank\Core\Api\Credentials;
use Resursbank\Core\Config\Config;
use Resursbank\Core\Entity\ResursbankPaymentMethod;
use Resursbank\Core\Exception\PaymentMethodException;
use Resursbank\Core\Logger\Logger;
use Resursbank\Core\Model\Api\Payment\Converter\OrderConverter;
use Resursbank\Core\Model\Api\Payment\Item;
use Resursbank\Core\Service\PaymentMethods;
use Resursbank\Ecommerce\Types\CheckoutType;
use Resursbank\RBEcomPHP\ResursBank;
use Resursbank\Simplified\Exception\ApiException;
use Resursbank\Simplified\Exception\InvalidGovIdException;
use Resursbank\Simplified\Exception\InvalidPaymentMethodException;
use Resursbank\Simplified\Exception\OrderDeniedException;
use Resursbank\Simplified\Exception\SimplifiedConstants;
use stdClass;

/**
 * API Connector for Resurs Bank (to handle order creation).
 *
 * @since 1.0.0
 */
class Api extends FrontController
{
    /**
     * API Connector from core.
     *
     * @var Connection
     *
     * @since 1.0.0
     */
    private $api;

    /**
     * Config from core used for fetching the ECom API with proper credentials.
     *
     * @var Config
     *
     * @since 1.0.0
     */
    private $config;

    /**
     * @var Credentials
     */
    private $credentials;

    /**
     * @var Logger
     *
     * @since 1.0.0
     */
    private $log;

    /**
     * @var OrderConverter
     *
     * @since 1.0.0
     */
    private $converter;

    /**
     * @var ResursBank
     *
     * @since 1.0.0
     */
    private $connection;

    /**
     * SuccessUrl (during signing and successful orders). Should, if possible, point to a landing page where
     * we can process with bookSignedPayment.
     *
     * @var string
     *
     * @since 1.0.0
     */
    private $successUrl;

    /**
     * Failure url on unsuccessful payments or signings.
     *
     * @var string
     *
     * @since 1.0.0
     */
    private $failUrl;

    /**
     * The order to handle in the API.
     *
     * @var Order
     *
     * @since 1.0.0
     */
    private $order;

    /**
     * Order reference at Resurs Bank side.
     *
     * @var string
     *
     * @since 1.0.0
     */
    private $orderReference;

    /**
     * Session for where getAddress are handled.
     *
     * @var Session
     *
     * @since 1.0.0
     */
    private $session;

    /**
     * @var PaymentMethods
     *
     * @since 1.0.0
     */
    private $paymentMethods;

    /**
     * @var \psrbsimplified
     */
    private $module;

    /**
     * For address data transitioning and matching.
     *
     * @var array
     *
     * @since 1.0.0
     */
    private $transitionAddressData = [
        'firstname' => 'firstName',
        'lastname' => 'lastName',
        'address1' => 'addressRow1',
        'address2' => 'addressRow2',
        'postcode' => 'postalCode',
        'city' => 'postalArea',
    ];

    /**
     * @param Logger $logger
     * @param Config $config
     * @param Connection $api
     * @param OrderConverter $converter
     * @param Credentials $credentials
     * @param Session $session
     * @param PaymentMethods $paymentMethods
     *
     * @since 1.0.0
     */
    public function __construct(
        Logger $logger,
        Config $config,
        Connection $api,
        OrderConverter $converter,
        Credentials $credentials,
        Session $session,
        PaymentMethods $paymentMethods
    ) {
        parent::__construct();
        $this->log = $logger;
        $this->config = $config;
        $this->api = $api;
        $this->credentials = $credentials;
        $this->converter = $converter;
        $this->session = $session;
        $this->paymentMethods = $paymentMethods;
        $this->module = \Module::getInstanceByName('psrbsimplified');
    }

    /**
     * Validate payment method and return info about it.
     *
     * @return ResursbankPaymentMethod
     *
     * @throws InvalidPaymentMethodException
     * @throws JsonException
     * @throws PaymentMethodException
     *
     * @since 1.0.0
     */
    private function getPaymentMethodInfo(): ResursbankPaymentMethod
    {
        $return = null;

        $methodList = $this->paymentMethods->getList();
        $id = Tools::getValue('id');

        /** @var ResursbankPaymentMethod $method */
        foreach ($methodList as $method) {
            if ($method->getId() === $id) {
                $return = $method;
            }
        }

        if (!$return instanceof ResursbankPaymentMethod) {
            throw new InvalidPaymentMethodException(
                'Invalid payment method!',
                SimplifiedConstants::INVALID_PAYMENT_METHOD
            );
        }

        return $return;
    }

    /**
     * Get renewed API (EComPHP) connection.
     *
     * @param ShopConstraint|null $shopConstraint
     *
     * @return ResursBank
     *
     * @throws Exception
     *
     * @since 1.0.0
     */
    public function getConnection(ShopConstraint $shopConstraint = null): ResursBank
    {
        // Connection will be renewed for each call.
        return $this->api->getConnection(
            $this->credentials->get($shopConstraint)
        );
    }

    /**
     * Prepare customer in EComPHP.
     *
     * @return Api
     *
     * @throws Exception
     *
     * @since 1.0.0
     */
    private function setCustomer(): Api
    {
        $paymentMethod = Tools::getValue('id');

        if (empty($paymentMethod)) {
            $this->errors[] = $this->module->l(
                'We can not proceed with your order since we miss valid information about it. Please try again.');
            $this->redirectWithNotifications($this->failUrl);
        }

        $customer = new Customer($this->order->id_customer);

        /**
         * Expecting an array. Always.
         *
         * @var array $billingAddress
         */
        $billingAddress = $customer->getSimpleAddress($this->order->id_address_invoice);

        /**
         * Expecting an array. Always.
         *
         * @var array $deliveryAddress
         */
        $deliveryAddress = $customer->getSimpleAddress($this->order->id_address_delivery);

        // When private persons are running through this procedure, we are normally fetching phone number
        // from the standard form field (our template) as phone_<METHODID>. However, if the customer is
        // company based (see below), we instead should fetch applicant data through the company specific
        // request fields. This is changed and fixed in the task marked below.
        // @see P17-285
        $phoneNumber = Tools::getValue(sprintf('phone_%s', $paymentMethod));
        $customerEmail = $customer->email;
        $fallbackGovernmentId = Tools::getValue(sprintf('government_id_%s', $paymentMethod));

        // Natural state for payments is to prepare for NATURAL, not LEGAL.
        // This value will however instantly become empty if the value returned belongs to a LEGAL payment and
        // then further down in this flow be properly handled.
        $governmentId = $this->getRequestValueByMethod('government_id', $paymentMethod);
        $contactGovernmentId = '';

        $this->setResursAddress($billingAddress, 'billing');
        if ($this->order->id_address_invoice !== $this->order->id_address_delivery) {
            $this->setResursAddress($deliveryAddress, 'delivery');
        }
        // Forcing delivery to be the same as billing, should not be necessary but we'll leave it here for now.
        //else {
        //    $this->setResursAddress($billingAddress, 'delivery');
        //}

        $customerType = empty($billingAddress['vat_number']) && empty($billingAddress['company']) ? 'NATURAL' : 'LEGAL';

        if ($customerType === 'LEGAL') {
            $contactGovernmentId = $this->session->getContactGovId();
            $governmentId = $this->session->getGovId();
            $fallbackGovernmentId = !empty($billingAddress['vat_number']) ? $billingAddress['vat_number'] : $customer->siret;
        }

        if (empty($governmentId) && !empty($fallbackGovernmentId)) {
            $governmentId = $fallbackGovernmentId;
        }

        // Validate and fetch current requested payment method.
        $currentPaymentMethod = $this->getPaymentMethodInfo();
        if ($currentPaymentMethod->getType() !== 'PAYMENT_PROVIDER' && empty($governmentId)) {
            throw new InvalidGovIdException(
                'GovernmentID is missing.',
                SimplifiedConstants::MISSING_GOVERNMENT_ID
            );
        }

        /**
         * Updates of setCustomer: Phone number and mobile number are allowed to be identical and therefore we also
         * add the same variable to setCustomer.
         *
         * @see P17-305
         */
        $this->connection->setCustomer(
            $governmentId,
            $phoneNumber,
            $phoneNumber,
            $customerEmail,
            $customerType,
            $contactGovernmentId
        );

        return $this;
    }

    /**
     * @param string $requestKey
     * @param string $methodName
     *
     * @return string
     *
     * @since 1.0.0
     */
    private function getRequestValueByMethod(string $requestKey, string $methodName): string
    {
        // getValue might return other values than strings.
        return (string) Tools::getValue(sprintf('%s_%s', $requestKey, $methodName));
    }

    /**
     * Prepare customer address for EComPHP.
     *
     * Note: $this->connection->setBillingByGetAddress() can also be if we want to enforce address via getAddress
     * instantly if we have a government id.
     *
     * @param array $address
     * @param string $addressType
     *
     * @since 1.0.0
     */
    private function setResursAddress(
        array $address,
        string $addressType
    ): void {
        switch ($addressType) {
            case 'billing':
                $this->connection->setBillingAddress(
                    sprintf(
                        '%s %s',
                        $address['firstname'] ?? '',
                        $address['lastname'] ?? ''
                    ),
                    $address['firstname'] ?? '',
                    $address['lastname'] ?? '',
                    $address['address1'] ?? '',
                    $address['address2'] ?? '',
                    $address['city'] ?? '',
                    $address['postcode'] ?? '',
                    $address['country_iso'] ?? ''
                );
                break;
            case 'delivery':
                $this->connection->setDeliveryAddress(
                    sprintf(
                        '%s %s',
                        $address['firstname'] ?? '',
                        $address['lastname'] ?? ''
                    ),
                    $address['firstname'] ?? '',
                    $address['lastname'] ?? '',
                    $address['address1'] ?? '',
                    $address['address2'] ?? '',
                    $address['city'] ?? '',
                    $address['postcode'] ?? '',
                    $address['country_iso'] ?? ''
                );
                break;
        }
    }

    /**
     * Prepare url for redirect to success (with bookSignedPayment).
     *
     * @param string $url
     *
     * @return $this
     *
     * @since 1.0.0
     */
    public function setSuccessUrl(string $url): Api
    {
        $this->successUrl = $url;

        return $this;
    }

    /**
     * Prepare url for redirect back to a failure page.
     *
     * @param string $url
     *
     * @return $this
     *
     * @since 1.0.0
     */
    public function setFailUrl(string $url): Api
    {
        $this->failUrl = $url;

        return $this;
    }

    /**
     * Setting that requires settings in configuration to work. Until this is set, we use the defaults.
     *
     * @see https://resursbankplugins.atlassian.net/browse/P17-86
     * @since 1.0.0
     */
    private function setFlags(): Api
    {
        $this->connection->setWaitForFraudControl(false);
        $this->connection->setAnnulIfFrozen(false);
        $this->connection->setFinalizeIfBooked(false);

        return $this;
    }

    /**
     * Prepare the success/fail landing pages here.
     * For success, the url set here is here bookSignedPayment will happen.
     *
     * @return Api
     *
     * @throws Exception
     *
     * @since 1.0.0
     */
    private function setSigning(): Api
    {
        $this->connection->setSigning(
            $this->successUrl,
            $this->failUrl
        );

        return $this;
    }

    /**
     * Prepare the order setup for EComPHP.
     *
     * @param Order $order
     *
     * @return $this
     *
     * @throws Exception
     *
     * @since 1.0.0
     */
    public function setOrder(Order $order): Api
    {
        // Instantiate a new connection per getConnection, but put it in a class global variable.
        // Several calls below are dependent on this.
        $this->connection = $this->getConnection(ShopConstraint::shop((int) $order->id_shop));
        $this->order = $order;

        $orderLines = $this->getOrderLines($order);

        $this->connection->setPreferredPaymentFlowService(CheckoutType::SIMPLIFIED_FLOW);
        $orderReference = $this->getPreferredOrderReference();
        if (!empty($orderReference)) {
            $this->connection->setPreferredId($orderReference);
        }

        $this
            ->setFlags()
            ->setSigning()
            ->setCustomer()
            ->setOrderLineData($orderLines);

        return $this;
    }

    /**
     * Prepare orderLines for EComPHP.
     *
     * Old method has been to push the orderLine-array into the payload. It is however strongly
     * recommended using the addOrderLine() instead, for a better overview of what we're actually
     * adding into EComPHP.
     *
     * @param array $orderLines this is expected as the array has an early validation
     *
     * @return Api
     *
     * @throws Exception
     *
     * @since 1.0.0
     * @noinspection SpellCheckingInspection
     */
    private function setOrderLineData(array $orderLines): Api
    {
        /**
         * Expected item, orderLine array has an early validation, so if we get here, it's because it has been
         * confirmed as proper.
         *
         * @var Item $item
         */
        foreach ($orderLines as $item) {
            if ($item instanceof Item) {
                $this->connection->addOrderLine(
                    $item->getArtNo(),
                    $item->getDescription(),
                    Tools::ps_round($item->getUnitAmountWithoutVat(), Context::getContext()->getComputingPrecision()),
                    $item->getVatPct(),
                    $item->getUnitMeasure(),
                    $item->getType(),
                    $item->getQuantity()
                );
            }
        }

        return $this;
    }

    /**
     * Prepare order reference for Resurs Bank.
     *
     * @param string $orderReference
     *
     * @return $this
     *
     * @since 1.0.0
     */
    public function setPreferredOrderReference(string $orderReference): Api
    {
        $this->orderReference = $orderReference;

        return $this;
    }

    /**
     * Get the preferred order reference set by Prestashop.
     *
     * This value is not actually necessary as EComPHP creates an own reference if this is missing, but
     * if it is missing, we will not be able to trace the proper order.
     *
     * @return string
     *
     * @throws Exception
     *
     * @since 1.0.0
     */
    private function getPreferredOrderReference(): string
    {
        if (!isset($this->orderReference) || empty($this->orderReference)) {
            throw new ApiException(
                'Order reference could not be retrieved from order.',
                SimplifiedConstants::NONEXISTENT_ORDER_REFERENCE
            );
        }

        return $this->orderReference;
    }

    /**
     * Placing order at Resurs Bank with EComPHP.
     * Nothing from this point should be returned, but redirected.
     *
     * @param Order $order
     *
     * @throws Exception
     *
     * @see https://test.resurs.com/docs/display/ecom/bookPaymentResult
     * @since 1.0.0
     */
    public function placeOrder(Order $order): void
    {
        if (isset($this->context->cart) && $this->context->cart instanceof Cart) {
            if ($this->context->cart->getOrderTotal()) {
                /** @var stdClass $createPaymentResponse */
                $createPaymentResponse = $this->connection->createPayment($this->getPaymentMethodId());
                // Passing $order through here, to make some updates on fly (P17-168).
                $this->handleBookingResponse(
                    $createPaymentResponse,
                    $order
                );
            } else {
                Tools::redirect($this->successUrl);
            }
        }
    }

    /**
     * Executed on success, where bookSignedPayment should occur.
     * We are returning the stdClass from the SoapResponse (bookPaymentResult) in case it is required somewhere else.
     *
     * @param string $orderReference
     * @param Order $order
     *
     * @throws OrderDeniedException
     * @throws PrestaShopException
     *
     * @see https://test.resurs.com/docs/display/ecom/bookPaymentResult
     * @since 1.0.0
     */
    public function bookSignedPayment(string $orderReference, Order $order): void
    {
        $this->handleBookingResponse(
            $this->getConnection()->bookSignedPayment($orderReference),
            $order
        );
    }

    /**
     * Handle responses from bookPayment and bookSignedPayment.
     * A place currently made for redirects only.
     *
     * @param stdClass $apiResponse
     * @param Order $order
     *
     * @throws OrderDeniedException
     * @throws PrestaShopException
     *
     * @since 1.0.0
     */
    private function handleBookingResponse(stdClass $apiResponse, Order $order): void
    {
        $this->log->info('Resurs Bank bookPayment/bookSignedPayment-response:');
        $this->log->info(print_r($apiResponse, true));

        switch ($this->getBookingStatus($apiResponse)) {
            case 'FINALIZED':
            case 'FROZEN':
            case 'BOOKED':
                // Passing $order through here, to make some updates on fly (P17-168).
                $this->getSynchronizedAddress($order);
                $this->log->info(
                    sprintf(
                        'Order %s booking status is %s. Redirect to %s',
                        $order->id,
                        $this->getBookingStatus($apiResponse),
                        $this->successUrl
                    ),
                    true
                );
                $this->session->unsetAll();
                Tools::redirect($this->successUrl);
                break;
            case 'SIGNING':
                $this->log->info(
                    sprintf(
                        'Order %s got SIGNING requirement, redirect to %s.',
                        $order->id,
                        $apiResponse->signingUrl
                    ),
                    true
                );
                $this->session->unsetAll();
                Tools::redirect($apiResponse->signingUrl);
                break;
            default:
                // This is where DENIED goes and all other unhandled events goes.
                throw new OrderDeniedException(
                    $this->module->l(
                        'Your order was declined by Resurs Bank. Please try again or contact our support'
                    ),
                    4444
                );
        }
    }

    /**
     * Synchronize order data with Resurs Bank API all getPayment.
     * Returns a boolean if we need to know that the update has been successful.
     *
     * @param Order $order
     *
     * @return bool
     *
     * @throws PrestaShopException
     *
     * @since 1.0.0
     */
    public function getSynchronizedAddress(Order $order): bool
    {
        $orderReference = $order->reference;
        try {
            // Slowing down synchronization processing so that databases can have an extra slight chance
            // to process before next update.
            sleep(2);
            $currentResursPayment = $this->getConnection()->getPayment($orderReference);

            if (isset($currentResursPayment->customer->governmentId) &&
                !empty($currentResursPayment->customer->governmentId)
            ) {
                try {
                    // This call has its own try-catch block so it won't break the address
                    // synchronization. In contrast to the prior module where this call was used,
                    // this function can be triggered more than once, depending on where the initial
                    // triggers start. The hook itself has to be controlled by the integration that
                    // chooses to use it and verify if it has already been handled.
                    Hook::exec(
                        'UpdateResursSsn',
                        [
                            'order' => $order,
                            'vat_number' => $currentResursPayment->customer->governmentId,
                        ]
                    );
                } catch (Exception $e) {
                    $this->log->exception($e);
                }
            }
        } catch (Exception $e) {
            $currentResursPayment = null;
            $this->log->exception($e);
        }

        return is_object($currentResursPayment) ?
            $this->setAddressByPrestaAddressList($order, $currentResursPayment) : false;
    }

    /**
     * Get internal address list from PrestaShop and make sure we can reuse an address object if it
     * already exists, based on Resurs Bank API call getPayment.
     *
     * NOTE: Address tag names in PrestaShop, like "delivery", "shipping", "invoice", etc, which can be
     * found in the database is not a tag that is actively used by PrestaShop, but by the customer.
     * It is rather a freetext field defined by the customer, than something that we can use to identify
     * the address type. In short terms; we can not match address types by this kind of tagging.
     *
     * @param Order $order
     * @param $resursPayment
     *
     * @return bool
     *
     * @throws PrestaShopException
     *
     * @since 1.0.0
     */
    private function setAddressByPrestaAddressList(Order $order, $resursPayment): bool
    {
        $reportUpdatedOrder = false;

        // Without the customer object present, we can not handle the data.
        if (isset($resursPayment->customer)) {
            $resursInvoiceData = $resursPayment->customer->address ?? null;
            $resursDeliveryData = $resursPayment->customer->deliveryAddress ?? null;
            $resursCustomerPhoneNumber = $resursPayment->customer->phone ?? '';

            // Code is very much cloned (and splitted up into smaller segments) from our prior Resurs Checkout module.
            $addressList = $order->getCustomer()->getSimpleAddresses($order->getCustomer()->id_lang);

            // In the old module we were blindly matching PrestaShop address data with Resurs Bank address data
            // even when the delivery address blocks missed out of the address request. In this case, we
            // will use PrestaShop address id's when Resurs address blocks are null (since that's how hey look when
            // they are missing).
            //
            // For the delivery address, the chances are much greater that the parts are missing
            // in the API request. This should be checked before we start using it.
            //
            // Values from getMatchingAddressId should ALWAYS be an integer or a null. If data is
            // returned as strings, something is wrong.

            //$billingDeliveryIsSame = $order->id_address_invoice === $order->id_address_delivery;

            // Scan for an address that contains the same data as the getAddress billing info.
            $billingAddressId = $resursInvoiceData !== null ? $this->getMatchingAddressId(
                $addressList,
                $resursInvoiceData,
                (int) $order->id_address_invoice,
                $resursCustomerPhoneNumber
            ) : (int) $order->id_address_invoice;

            // Scan for an address that contains the same data as the getAddress deliveryAddress info.
            $deliveryAddressId = $resursDeliveryData !== null ? $this->getMatchingAddressId(
                $addressList,
                $resursInvoiceData,
                (int) $order->id_address_delivery,
                $resursCustomerPhoneNumber
            ) : (int) $order->id_address_delivery;

            // If billing and shipping is not the same (use same address unchecked), this means customer wants another
            // billing. Since this about billing addresses, we won't be able to fully support this action due
            // to a second address synchronization with getPayment after the final payment.
            //
            // If billingAddressId/deliveryAddressId is null or that id is different to the id's located in
            // the $order-object, the below code is set to update the address block. If the we're matching
            // with is null, this however mans, that updateAddress should run with the address id from the
            // order instead. That way, we can avoid to duplicate an address each time it is used (which happens
            // to logged in users).
            //
            // If the above address id's are the same as the data we get from PrestaShop, we don't have to do
            // anything.
            //
            // @see P17-285
            // @see P17-363
            if ($billingAddressId === null || $billingAddressId !== (int) $order->id_address_invoice) {
                $properBillingAddressId = $billingAddressId ?? $order->id_address_invoice;
                // Create a new address for billing.
                $updateBillingId = $this->updateAddress(
                    new Address($properBillingAddressId),
                    $resursInvoiceData
                );
                // $this->context->customer->logged
                if ($updateBillingId !== (int) $order->id_address_invoice) {
                    $oldInvoiceId = $order->id_address_invoice;
                    $order->id_address_invoice = $updateBillingId;
                    // Slowing down synchronization processing so that databases can have an extra slight chance
                    // to process before next update.
                    sleep(3);
                    $order->save(true);
                    $reportUpdatedOrder = true;
                    $this->log->info(
                        sprintf(
                            'Update order: Billing address id changed from %s to %s.',
                            $oldInvoiceId,
                            $updateBillingId
                        )
                    );
                }
            }

            // Only update shipping address if there is a properly set id to do this, unless, billing and
            // shipping is equal.
            if ($deliveryAddressId === null ||
                ($deliveryAddressId > 0 && $deliveryAddressId !== (int) $order->id_address_delivery)
            ) {
                $properDeliveryAddressId = $deliveryAddressId ?? $order->id_address_delivery;
                // Create a new address for shipping.
                $updateDeliveryId = $this->updateAddress(
                    new Address($properDeliveryAddressId),
                    $resursDeliveryData
                );
                // Update delivery address if there is a specific delivery address id for it, or if
                // the address data is the same as the billing address (specifically when use_same_address has
                // been set in the checkout forms, the delivery address should be the same as the billing address).
                if ($updateDeliveryId !== (int) $order->id_address_delivery) {
                    $oldInvoiceId = $order->id_address_delivery;
                    $order->id_address_delivery = $updateDeliveryId;
                    // Slowing down synchronization processing so that databases can have an extra slight chance
                    // to process before next update.
                    sleep(3);
                    $order->save(true);
                    $reportUpdatedOrder = true;
                    $this->log->info(
                        sprintf(
                            'Update order: Delivery address id changed from %s to %s.',
                            $oldInvoiceId,
                            $updateDeliveryId
                        )
                    );
                }
            }

            if ($reportUpdatedOrder) {
                $this->log->info('Synchronized order data with Resurs Bank customer information.');
            }
        }

        return $reportUpdatedOrder;
    }

    /**
     * @param Address $address
     * @param $resursAddressData
     *
     * @return int
     *
     * @throws PrestaShopException
     * @since 1.0.0
     */
    private function updateAddress(Address $address, $resursAddressData): int
    {
        // Default way to handle address, only necessary when the method becomes "add".
        $addressRequestType = 'update';
        foreach ($this->transitionAddressData as $fromKey => $toKey) {
            if (isset($resursAddressData->{$toKey})) {
                if ($address->{$fromKey} !== $resursAddressData->{$toKey}) {
                    $address->{$fromKey} = $resursAddressData->{$toKey};
                    $addressRequestType = 'add';
                }
                $this->log->info(
                    sprintf('Address block update (%s): %s', $fromKey, $resursAddressData->{$toKey})
                );
            }
        }

        if ($addressRequestType === 'add') {
            $updatedAddress = $address->add();
        } else {
            $updatedAddress = $address->save();
        }

        $this->log->info(sprintf(
            'Address update status: %s. Address synchronization set to "%s".',
            (isset($updatedAddress) && $updatedAddress ? 'Changed' : 'Unchanged'),
            $addressRequestType
        ));

        return (int) $address->id;
    }

    /**
     * Try to find a matching address data id from PrestaShop address list. Matches can be reused
     * by the module. If no matches is found, the module will be able to create a new address object.
     *
     * @param array $addressList
     * @param $resursAddress
     * @param int $actualAddressId
     * @param string $resursCustomerPhoneNumber
     *
     * @return int|null
     *
     * @since 1.0.0
     */
    private function getMatchingAddressId(
        array $addressList,
        $resursAddress,
        int $actualAddressId,
        string $resursCustomerPhoneNumber
    ) {
        /**
         * Associative array for presta shop fields and how they are named in a getAddress-object.
         *
         * @var array $matchData
         */
        $matchData = [
            'firstname' => 'firstName',
            'lastname' => 'lastName',
            'address1' => 'addressRow1',
            'address2' => 'addressRow2',
            'postcode' => 'postalCode',
            'city' => 'postalArea',
        ];

        // Returning this as null, not an integer so that the module knows that it's neither a string nor integer.
        $currentAddressId = null;
        $actualId = null;
        if (is_array($addressList) && count($addressList)) {
            foreach ($addressList as $addressId => $prestaAddressItem) {
                $isSameAddress = 0;
                // Making sure that the phone number are also included in the address matching.
                if ($this->isResursPhone($resursCustomerPhoneNumber, $prestaAddressItem)) {
                    ++$isSameAddress;
                }
                foreach ($matchData as $matchItemFrom => $matchItemTo) {
                    if (isset($resursAddress->{$matchItemTo}) &&
                        $prestaAddressItem[$matchItemFrom] === $resursAddress->{$matchItemTo}
                    ) {
                        ++$isSameAddress;
                    }
                }

                // Expecting the same address to have at least 5 exact matches (since the last row that *could* mismatch
                // is the one for addressRow2). If we have this kind of match, we are considered good to go with it.
                if ($isSameAddress >= 6) {
                    $currentAddressId = $actualAddressId;
                    if ($currentAddressId === $actualAddressId) {
                        $actualId = $addressId;
                    }
                }

                if ($actualId !== null && $currentAddressId !== null && (int) $currentAddressId > 0) {
                    $currentAddressId = $actualId;
                    break;
                }
            }
        }

        // If returned as null, there is no matching address object at all. This should send a clearance
        // to the module that we can create a new address object. If value is not null they should be cast
        // to integers.
        return $currentAddressId !== null ? (int) $currentAddressId : null;
    }

    /**
     * Match customer phone number with phone number from Resurs Bank, simple mode.
     * Legacy function from the RCO-version.
     *
     * @param string $resursCustomerPhoneNumber
     * @param array $prestaCustomer
     *
     * @return bool
     *
     * @since 1.0.0
     */
    private function isResursPhone(string $resursCustomerPhoneNumber, array $prestaCustomer): bool
    {
        $return = false;
        $phoneCheck = ['phone', 'phone_mobile'];
        foreach ($phoneCheck as $phoneType) {
            if (isset($prestaCustomer[$phoneType]) &&
                $this->isMatchingPhoneNumbers($resursCustomerPhoneNumber, $prestaCustomer[$phoneType])) {
                $return = true;
                break;
            }
        }

        return $return;
    }

    /**
     * Match two phone numbers with each other where Resurs Bank phone number is stripped from country code.
     * Legacy function from the RCO-version.
     *
     * @param string $resursCustomerPhoneNumber
     * @param string $prestaCustomerPhone
     *
     * @return bool
     *
     * @since 1.0.0
     */
    private function isMatchingPhoneNumbers(string $resursCustomerPhoneNumber, string $prestaCustomerPhone): bool
    {
        $countryCodeFixedPhoneNumber = preg_replace('/^0/', '', $prestaCustomerPhone);

        return (bool) preg_match(sprintf('/%s/', $countryCodeFixedPhoneNumber), $resursCustomerPhoneNumber);
    }

    /**
     * Get the status from bookPaymentResult.
     *
     * Method is written to make sure that nothing halts at this point, since the typed responses is usually
     * not to trust.
     *
     * @param stdClass $apiResponse
     *
     * @return string
     *
     * @see https://test.resurs.com/docs/display/ecom/bookPaymentResult
     * @since 1.0.0
     */
    private function getBookingStatus(stdClass $apiResponse): string
    {
        return $apiResponse->bookPaymentStatus ?? '';
    }

    /**
     * Get the payment method id from PrestaShop submit data.
     *
     * @return string
     *
     * @throws Exception
     *
     * @since 1.0.0
     */
    private function getPaymentMethodId(): string
    {
        $paymentMethodId = Tools::getValue('id');

        if (!isset($paymentMethodId)) {
            throw new ApiException(
                'Payment method ID is not set!',
                SimplifiedConstants::MISSING_PAYMENT_METHOD_ID
            );
        }

        return $paymentMethodId;
    }

    /**
     * Prepare and return order lines from order. The data her is returned as an array as an Item.
     *
     * @param Order $order
     *
     * @return Item[]
     *
     * @throws Exception
     *
     * @since 1.0.0
     */
    public function getOrderLines(Order $order): array
    {
        return $this->converter->convert($order);
    }
}
