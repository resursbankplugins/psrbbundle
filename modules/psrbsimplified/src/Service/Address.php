<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Simplified\Service;

use Exception;
use function is_object;
use PrestaShop\PrestaShop\Adapter\Entity\Context;
use Resursbank\Core\Exception\ApiDataException;
use Resursbank\Core\Model\Api\Address as ApiAddress;
use Resursbank\Core\Service\Api as CoreApi;
use Resursbank\Simplified\Model\Api\CheckoutAddress;

/**
 * Handles address fetching and conversion to internal address objects.
 */
class Address
{
    /**
     * Customer type for company.
     *
     * @var string
     */
    public const CUSTOMER_TYPE_COMPANY = 'LEGAL';

    /**
     * Customer type for private citizens.
     *
     * @var string
     */
    public const CUSTOMER_TYPE_PRIVATE = 'NATURAL';

    /**
     * @var Api
     *
     * @since 1.0.0
     */
    private $api;

    /**
     * @var CoreApi
     *
     * @since 1.0.0
     */
    private $coreApi;

    /**
     * Module context.
     *
     * @var Context
     *
     * @since 1.0.0
     */
    private $context;

    /**
     * @param Api $api
     * @param CoreApi $coreApi
     */
    public function __construct(
        Api $api,
        CoreApi $coreApi
    ) {
        $this->api = $api;
        $this->coreApi = $coreApi;
        $this->context = Context::getContext();
    }

    /**
     * Fetch address from the API using either government id (Sweden) or a phone
     * number (Norway).
     *
     * @param string $identifier
     * @param bool $isCompany
     *
     * @return ApiAddress
     *
     * @throws ApiDataException
     * @throws Exception
     */
    public function fetch(
        string $identifier,
        bool $isCompany
    ): ApiAddress {
        // What country the store is associated with.
        $country = $this->context->country->iso_code;

        // Establish API connection.
        $connection = $this->api->getConnection();

        // Customer type (NATURAL|LEGAL).
        $type = $this->getCustomerType($isCompany);

        // Raw address data resolved from the API.
        $address = null;

        // Fetch address data from the API.
        switch ($country) {
            case 'SE':
                $address = $connection->getAddress($identifier, $type);
                break;
            case 'NO':
                $address = $connection->getAddressByPhone($identifier, $type);
                break;
        }

        // Validate return value.
        if (!is_object($address)) {
            throw new ApiDataException(__('Failed to fetch address.'));
        }

        // Convert and return address data.
        return $this->coreApi->toAddress(
            $address,
            $isCompany,
            ($country === 'NO' ? $identifier : '')
        );
    }

    /**
     * @param ApiAddress $address
     *
     * @return CheckoutAddress
     *
     * @throws ApiDataException
     */
    public function toCheckoutAddress(
        ApiAddress $address
    ): CheckoutAddress {
        return new CheckoutAddress(
            $address->getFirstName(),
            $address->getLastName(),
            $address->getPostalArea(),
            $address->getPostalCode(),
            $address->getCountry(),
            $address->getAddressRow1(),
            $address->getAddressRow2(),
            $address->getIsCompany() ?
                $address->getFullName() :
                '',
            $address->getTelephone(),
            $address->getCountryId()
        );
    }

    /**
     * Returns the customer type based on a boolean value which states what
     * type you're looking for.
     *
     * @param bool $isCompany
     *
     * @return string
     */
    public function getCustomerType(
        bool $isCompany
    ): string {
        return $isCompany ?
            self::CUSTOMER_TYPE_COMPANY :
            self::CUSTOMER_TYPE_PRIVATE;
    }
}
