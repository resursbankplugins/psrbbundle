<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Simplified\Service;

use function is_bool;
use function is_string;
use PrestaShop\PrestaShop\Adapter\Entity\Context;

/**
 * Session handling.
 */
class Session
{
    /**
     * Prefix for all session keys for this module.
     *
     * @var string
     */
    public const KEY_PREFIX = 'psrbsimplified_';

    /**
     * Key to store and retrieve the customer's contact government ID.
     *
     * @var string
     */
    public const KEY_CONTACT_GOV_ID =
        self::KEY_PREFIX . 'contact_government_id';

    /**
     * Key to store and retrieve the customer's government ID.
     *
     * @var string
     */
    public const KEY_GOV_ID = self::KEY_PREFIX . 'government_id';

    /**
     * Key to store and retrieve customer type (NATURAL | LEGAL).
     *
     * @var string
     */
    public const KEY_IS_COMPANY = self::KEY_PREFIX . 'is_company';

    /**
     * Key to store and retrieve the payment's signing URL. The URL is utilised
     * to redirect the customer to the payment gateway. The URL is obtained
     * after a payment session has been created at Resurs Bank through the API.
     *
     * @var string
     */
    public const KEY_PAYMENT_SIGNING_URL = self::KEY_PREFIX . 'signing_url';

    /**
     * Key to store and retrieve the payment / payment session ID (the ID at
     * Resurs Bank identifying to the payment session).
     *
     * @var string
     */
    public const KEY_PAYMENT_ID = self::KEY_PREFIX . 'payment_id';

    /**
     * @var string
     */
    public const KEY_FETCHED_ADDRESS = self::KEY_PREFIX . 'fetched_address';

    /**
     * Module context.
     *
     * @var Context
     *
     * @since 1.0.0
     */
    private $context;

    public function __construct()
    {
        $this->context = Context::getContext();
    }

    /**
     * Store a customer's SSN/Org nr. in the session.
     *
     * @param string $govId - Must be a valid Swedish SSN/Org. number.
     *
     * @return self
     */
    public function setGovId(
        string $govId
    ): self {
        $this->context->cookie->{self::KEY_PREFIX . self::KEY_GOV_ID} =
            $govId;

        return $this;
    }

    /**
     * @return string|null - Null if a value cannot be found
     */
    public function getGovId(): ?string
    {
        $sessionValue = $this->context
            ->cookie
            ->{self::KEY_PREFIX . self::KEY_GOV_ID};

        return is_string($sessionValue) ? $sessionValue : null;
    }

    /**
     * @return self
     */
    public function unsetGovId(): self
    {
        unset($this->context->cookie->{self::KEY_PREFIX . self::KEY_GOV_ID});

        return $this;
    }

    /**
     * Store fetched address data in session.
     *
     * @param string $address
     *
     * @return self
     */
    public function setFetchedAddress(
        string $address
    ): self {
        $this->context->cookie->{self::KEY_PREFIX . self::KEY_FETCHED_ADDRESS} =
            $address;

        return $this;
    }

    /**
     * @return string|null - Null if a value cannot be found
     */
    public function getFetchedAddress(): ?string
    {
        $sessionValue = $this->context
            ->cookie
            ->{self::KEY_PREFIX . self::KEY_FETCHED_ADDRESS};

        return is_string($sessionValue) ? $sessionValue : null;
    }

    /**
     * @return self
     */
    public function unsetFetchedAddress(): self
    {
        unset($this->context
                ->cookie
                ->{self::KEY_PREFIX . self::KEY_FETCHED_ADDRESS});

        return $this;
    }

    /**
     * Stores a customer's contact government ID in the session. Required for
     * company customers, personal SSN of a company reference.
     *
     * @param string $govId - Must be a valid SSN of a supported country
     *
     * @return self
     */
    public function setContactGovId(
        string $govId
    ): self {
        $this->context
            ->cookie
            ->{self::KEY_PREFIX . self::KEY_CONTACT_GOV_ID} = $govId;

        return $this;
    }

    /**
     * @return string|null - Null if a value cannot be found
     */
    public function getContactGovId(): ?string
    {
        $sessionValue = $this->context
            ->cookie
            ->{self::KEY_PREFIX . self::KEY_CONTACT_GOV_ID};

        return is_string($sessionValue) ? $sessionValue : null;
    }

    /**
     * @return self
     */
    public function unsetContactGovId(): self
    {
        unset($this->context
                ->cookie
                ->{self::KEY_PREFIX . self::KEY_CONTACT_GOV_ID});

        return $this;
    }

    /**
     * Stores customer type in the session.
     *
     * @param bool $isCompany
     *
     * @return self
     */
    public function setIsCompany(
        bool $isCompany
    ): self {
        $this->context
            ->cookie
            ->{self::KEY_PREFIX . self::KEY_IS_COMPANY} = $isCompany;

        return $this;
    }

    /**
     * @return bool|null - Null if a value cannot be found
     */
    public function getIsCompany(): ?bool
    {
        $sessionValue = $this->context
            ->cookie
            ->{self::KEY_PREFIX . self::KEY_IS_COMPANY};

        return is_string($sessionValue) ? (bool) $sessionValue : null;
    }

    /**
     * @return self
     */
    public function unsetIsCompany(): self
    {
        unset($this->context->cookie->{self::KEY_PREFIX . self::KEY_IS_COMPANY});

        return $this;
    }

    /**
     * Stores payment signing (gateway) URL in session. Redirect URL at order
     * placement to perform payment.
     *
     * @param string $url
     *
     * @return self
     */
    public function setPaymentSigningUrl(
        string $url
    ): self {
        $this->context
            ->cookie
            ->{self::KEY_PREFIX . self::KEY_PAYMENT_SIGNING_URL} = $url;

        return $this;
    }

    /**
     * @return string|null - Null if a value cannot be found
     */
    public function getPaymentSigningUrl(): ?string
    {
        $sessionValue = $this->context
            ->cookie
            ->{self::KEY_PREFIX . self::KEY_PAYMENT_SIGNING_URL};

        return is_string($sessionValue) ? $sessionValue : null;
    }

    /**
     * @return self
     */
    public function unsetPaymentSigningUrl(): self
    {
        unset($this->context
                ->cookie
                ->{self::KEY_PREFIX . self::KEY_PAYMENT_SIGNING_URL});

        return $this;
    }

    /**
     * Stores payment session ID in PHP session.
     *
     * @param string $paymentId
     *
     * @return self
     */
    public function setPaymentId(
        string $paymentId
    ): self {
        $this->context
            ->cookie
            ->{self::KEY_PREFIX . self::KEY_PAYMENT_ID} = $paymentId;

        return $this;
    }

    /**
     * @return string|null - Null if a value cannot be found
     */
    public function getPaymentId(): ?string
    {
        $sessionValue = $this->context
            ->cookie
            ->{self::KEY_PREFIX . self::KEY_PAYMENT_ID};

        return is_string($sessionValue) ? $sessionValue : null;
    }

    /**
     * @return self
     */
    public function unsetPaymentId(): self
    {
        unset($this->context->cookie->{self::KEY_PREFIX . self::KEY_PAYMENT_ID});

        return $this;
    }

    /**
     * Unset all the customer's personal information stored in session.
     *
     * Note that any information regarding the payment (if one has been created)
     * is not removed when using this method.
     *
     * @return self
     */
    public function unsetCustomerInfo(): self
    {
        return $this->unsetContactGovId()
            ->unsetIsCompany()
            ->unsetGovId();
    }

    /**
     * Unsets all payment information from the session.
     *
     * Note that any information regarding the customer's personal information
     * (government ID, customer type etc.) will not be removed when using this
     * method.
     *
     * @return self
     */
    public function unsetPaymentInfo(): self
    {
        return $this->unsetPaymentSigningUrl()
            ->unsetPaymentId();
    }

    /**
     * Unsets every key in the session applied through this class.
     *
     * @return self
     */
    public function unsetAll(): self
    {
        return $this->unsetPaymentInfo()
            ->unsetCustomerInfo();
    }
}
