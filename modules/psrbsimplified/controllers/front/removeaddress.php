<?php
/*
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

use Resursbank\Core\Logger\Logger;
use Resursbank\Simplified\Service\Session;

/**
 * Removes a fetched address from the session.
 */
class psrbsimplifiedRemoveaddressModuleFrontController extends ModuleFrontController
{
    /**
     * @throws Exception
     *
     * @see FrontController::postProcess()
     */
    public function postProcess(): void
    {
        parent::postProcess();
        echo json_encode($this->removeAddress(), JSON_THROW_ON_ERROR);
    }

    /**
     * @return array
     *
     * @throws Exception
     */
    private function removeAddress(): array
    {
        /** @var Logger $log */
        $log = $this->get('resursbank.simplified.logger');

        /** @var Session $session */
        $session = $this->get('resursbank.simplified.session');

        $data = [
            'error' => [
                'message' => '',
            ],
        ];

        try {
            $session->setGovId('');
            $session->setFetchedAddress('');
        } catch (Exception $e) {
            $log->exception($e);

            $errorMessage = 'Something went wrong when removing the fetched ' .
                'address. Please try again.';

            // Display friendly (safe) error message to customer.
            $data['error']['message'] = $errorMessage;
        }

        return $data;
    }
}
