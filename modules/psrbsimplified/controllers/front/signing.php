<?php

/** @noinspection PhpCSValidationInspection */
/* @noinspection SpellCheckingInspection */

/*
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

use Resursbank\Simplified\Service\OrderHandler;

/**
 * Signing control page. This is where we put returning customers after signing.
 * The purpose is to finally land at a success page or failure page.
 *
 * @since 1.0.0
 */
class psrbsimplifiedSigningModuleFrontController extends ModuleFrontController
{
    /**
     * @throws Exception
     *
     * @see FrontController::postProcess()
     * @since 1.0.0
     */
    public function postProcess(): void
    {
        /** @var OrderHandler $orderHandler */
        $orderHandler = $this->get('resursbank.simplified.orderhandler');
        $orderHandler->handleCustomerSigning();

        // This is a point that we are supposed to never reach. If we for some reason gets this far
        // and there is nothing that ends the process here, PrestaShop will automatically start throwing
        // Smarty exceptions since there is no content to display.

        if (count($this->errors)) {
            // getFailUrl here is built to make it possible to redirect to a failurl even if there is a broken
            // cart (I.E. if someone tries to handle an order without a present cart).
            $this->redirectWithNotifications($orderHandler->getFailUrl(true));
        }
        exit;
    }
}
