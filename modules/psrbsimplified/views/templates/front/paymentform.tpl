<!--
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */
-->

<div class="rb-pi">
    {if $isFirst}
        <script>
            window.addEventListener('DOMContentLoaded', function () {
                var CheckoutModel = psrbsimplified.Model.Checkout;

                CheckoutModel.phone('{$phone}');
                CheckoutModel.country('{$country}');
                CheckoutModel.isCompany({$isCompany|var_export:true});

                CheckoutModel.govId(
                    psrbsimplified.govId ? psrbsimplified.govId : ''
                );
            });
        </script>
    {/if}
    <div class="rb-pi-row label">
    </div>
    <form id="{$formId}" method="POST" action="{$actionUrl}">
        {$formFields nofilter}
        <script>
            window.addEventListener('DOMContentLoaded', function () {
                window.psrbsimplified.View.Method({
                    formId: '{$formId}'
                });
            });
        </script>
    </form>
    {if $useReadMore}
        <div class="rb-pi-row label">
            <div class="rb-pi-col">
                <div class="rb-read-more">
                    {include 'module:psrbcore/views/templates/front/modals/read-more.tpl'}
                    <script>
                        window.addEventListener('DOMContentLoaded', function () {
                            window.psrbcore.View.ReadMore({
                                remodalId: '{$remodalId}',
                                contentId: '{$contentId}',
                                loaderId: '{$loaderId}',
                                methodCode: '{$method}',
                                price: window.prestashop.cart.totals.total.amount,
                                url: window.psrbcore.getCostOfPurchaseUrl
                            });
                        });
                    </script>
                </div>
            </div>
        </div>
        {else}
        <div class="rb-pi-col">
          {$infoLabel nofilter}
        </div>
    {/if}
    {if $isLast}
        <script>
            window.addEventListener('DOMContentLoaded', function () {
                window.psrbsimplified.View.PlaceOrder.init();
            });
        </script>
    {/if}
</div>
