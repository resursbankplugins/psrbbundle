/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

/**
 * @param {jQuery} $
 * @param {RbsLib.FetchAddress} FetchAddress
 * @param {RbsLib.CheckoutConfig} CheckoutConfig
 * @param {RbsLib.Credentials} Credentials
 * @param {RbsLib.Checkout} CheckoutLib
 * @param {RbsLib.Storage} StorageLib
 * @param {RbsLib.Session} SessionLib
 * @param {RbsModel.Checkout} CheckoutModel
 */
(function (
    $,
    FetchAddress,
    CheckoutConfig,
    Credentials,
    CheckoutLib,
    StorageLib,
    SessionLib,
    CheckoutModel
) {
    'use strict';

    /**
     * @type {string}
     * @constant
     */
    var CUSTOMER_TYPE_PERSON = 'person';

    /**
     * @type {string}
     * @constant
     */
    var CUSTOMER_TYPE_COMPANY = 'company';

    /**
     * @type {jQuery}
     */
    var fetchBtnEl;

    /**
     * @type {jQuery}
     */
    var identifierInputEl;

    /**
     * @type {jQuery}
     */
    var customerTypeEls;

    /**
     * @type {jQuery}
     */
    var errorEl;

    /**
     * @type {jQuery}
     */
    var inputLabelEl;

    /**
     * @type {string}
     */
    var selectedCustomerType = StorageLib.getIsCompany() ?
        CUSTOMER_TYPE_COMPANY :
        CUSTOMER_TYPE_PERSON;
    var isFetchingAddress = false;
    var invalidIdentifierError = window.psrbsimplified['translations']['invalidIdentifierError'];
    var invalidPhoneNumber = window.psrbsimplified['translations']['invalidPhoneNumber'];
    var failedToFetchAddressError = window.psrbsimplified['translations']['failedToFetchAddressError'];
    var isAddressApplied = false;
    var inputLabelCompany = window.psrbsimplified['translations']['inputLabelCompany'];
    var inputLabelPerson = window.psrbsimplified['translations']['inputLabelPerson'];

    /**
     * @param {HTMLInputElement} el
     * @param {boolean} isCompany
     */
    function selectCustomerTypeRadio(el, isCompany) {
        el.checked = (isCompany && el.value === CUSTOMER_TYPE_COMPANY) ||
            (!isCompany && el.value === CUSTOMER_TYPE_PERSON);
    }

    /**
     * Shows/hides the error element.
     *
     * @param {boolean} state
     */
    function displayErrorMessage(state) {
        if (state) {
            errorEl.show();
        } else {
            errorEl.hide();
        }
    }

    /**
     * Updates the error element for the input with information.
     *
     * @param {string} msg
     */
    function setErrorMessage(msg) {
        errorEl.text(msg);
    }

    /**
     * Sets the error state for the identifier input, updating its appearance
     * to match the state.
     *
     * @param {boolean} state
     */
    function setIdentifierInputErrorState(state) {
        if (state) {
            identifierInputEl.addClass('error');
        } else {
            identifierInputEl.removeClass('error');
        }
    }

    /**
     * Helper function to set the error message for the widget and updating its
     * appearance.
     *
     * Will take effect when a non-empty string is supplied. An empty string
     * will remove the error message and styling.
     *
     * @param {string} msg
     */
    function setError(msg) {
        if (msg === '') {
            setErrorMessage('');
            displayErrorMessage(false);
            setIdentifierInputErrorState(false);
        } else {
            setErrorMessage(msg);
            displayErrorMessage(true);
            setIdentifierInputErrorState(true);
        }
    }

    /**
     * If the customer has selected to be a company when fetching an address.
     *
     * @param {string} value
     * @returns {boolean}
     */
    function isCompanyCustomer(value) {
        return value === CUSTOMER_TYPE_COMPANY;
    }

    /**
     * Updates the input label. This should be used when the customer type
     * changes.
     */
    function setInputLabel() {
        inputLabelEl.text(
            CheckoutModel.isCompany() ?
                inputLabelCompany :
                inputLabelPerson
        );
    }

    /**
     * Whether default country is Sweden.
     *
     * @type {boolean}
     */
    function isSweden() {
        return CheckoutConfig.getDefaultCountryId() === 'SE';
    }

    /**
     * Whether default country is Norway.
     *
     * @type {boolean}
     */
    function isNorway() {
        return CheckoutConfig.getDefaultCountryId() === 'NO';
    }

    /**
     * Retrieve customer identifier value.
     *
     * @returns {string}
     */
    function getIdentifier() {
        var result = '';

        if (isSweden()) {
            result = CheckoutModel.govId();
        } else if (isNorway()) {
            result = CheckoutModel.phone();
        }

        return result;
    }

    /**
     * Validates the applied ID-number and displays relevant error messages.
     *
     * @returns {boolean}
     */
    function validateId() {
        var valid = Credentials.validate(
            CheckoutModel.govId(),
            'SE',
            CheckoutModel.isCompany()
        );

        if (!valid) {
            setError(invalidIdentifierError);
        }

        return valid;
    }

    /**
     * Validate phone number.
     *
     * @returns {boolean}
     */
    function validatePhone() {
        var valid = Credentials.validatePhone(
            CheckoutModel.phone(),
            'NO'
        );

        if (!valid) {
            setError(invalidPhoneNumber);
        }

        return valid;
    }

    /**
     * Validate the identifier utilised to fetch address data.
     *
     * @returns {boolean}
     */
    function validate() {
        var result = false;

        if (isSweden()) {
            result = validateId();
        } else if (isNorway()) {
            result = validatePhone();
        }

        return result;
    }

    /**
     * Sets the disabled state of the fetch button.
     *
     * @param {boolean} state
     */
    function setFetchBtnDisabled(state) {
        fetchBtnEl.prop('disabled', state);
    }

    /**
     * Sets the disabled state of the identifier field.
     *
     * @param {boolean} state
     */
    function setIdentifierInputDisabled(state) {
        identifierInputEl.prop('disabled', state);
    }

    /**
     * Sets the disabled state of the customer type radios.
     *
     * @param {boolean} state
     */
    function setCustomerTypeDisabled(state) {
        customerTypeEls.prop('disabled', state);
    }

    /**
     * Disables all input controls when fetching an address.
     */
    function disableInputs() {
        setFetchBtnDisabled(true);
        setIdentifierInputDisabled(true);
        setCustomerTypeDisabled(true);
    }

    /**
     * Enables all input controls if the fetching operation failed.
     */
    function enableInputs() {
        setFetchBtnDisabled(false);
        setIdentifierInputDisabled(false);
        setCustomerTypeDisabled(false);
    }

    /**
     * Moves the fetch address widget to its proper location.
     *
     * This workaround is necessary because Prestashop does not contain a hook
     * that we can use, and we cannot add it to a template block without
     * copying core templates, overriding blocks, or extending controller
     * classes which would increase complexity.
     */
    function insertFetchAddressWidget() {
        $('#checkout-guest-form')
            .prepend($('#psrbsimplified-fetch-address'));
    }

    /**
     * Updates the looks and functionality of the button to remove a fetched
     * address when pressed, emptying every field that was updated from the
     * fetch address operation.
     */
    function renderResetButton() {
        fetchBtnEl.text(window.psrbsimplified['translations']['btnNotYou']);
        fetchBtnEl.off('click', fetchAddress);
        fetchBtnEl.on('click', removeAddress);
    }

    /**
     * Updates the looks and functionality of the button to fetch an address
     * when pressed.
     */
    function renderFetchAddressButton() {
        fetchBtnEl.text(window.psrbsimplified['translations']['btnFetchAddress']);
        fetchBtnEl.off('click', removeAddress);
        fetchBtnEl.on('click', fetchAddress);
    }

    /**
     * Empties the identifier field.
     */
    function emptyIdentifierInput() {
        identifierInputEl.val('');
    }

    /**
     * Callback used when the fetch address request was successful.
     *
     * @param {RbsLib.FetchAddress.Response} response
     */
    function onFetchAddressDone(response) {
        if (response.error.message !== '') {
            setError(response.error.message);
            enableInputs();
        } else if (Object.keys(response.address).length > 0) {
            CheckoutLib.applyAddress(response.address);

            isAddressApplied = true;

            renderResetButton();
            setCustomerTypeDisabled(true);
            setIdentifierInputDisabled(true);
        }
    }

    /**
     * Callback used when the fetch address request fails.
     */
    function onFetchAddressFail() {
        enableInputs();
        setError(failedToFetchAddressError);
    }

    /**
     * Callback used when the fetch address request completes.
     */
    function onFetchAddressAlways() {
        isFetchingAddress = false;
        setFetchBtnDisabled(false);
    }

    /**
     * Removes the fetched address (if an address has been fetched), resetting
     * address fields to their initial values. The ID-number input will also be
     * cleared.
     */
    function removeAddress() {
        isAddressApplied = false;

        FetchAddress.removeAddress();
        CheckoutLib.removeAddress();
        CheckoutModel.govId('');

        renderFetchAddressButton();
        emptyIdentifierInput();
        enableInputs();
    }

    /**
     * Fetches the address of the given SSN/Org. nr. If the number is invalid,
     * the address cannot be fetched and an error message will be displayed
     * underneath the ID-number input.
     */
    function fetchAddress() {
        if (!isFetchingAddress && validate()) {
            setError('');
            disableInputs();
            isFetchingAddress = true;

            FetchAddress
                .fetchAddress(
                    getIdentifier(),
                    CheckoutModel.isCompany()
                )
                .done(onFetchAddressDone)
                .fail(onFetchAddressFail)
                .always(onFetchAddressAlways);
        }
    }

    /**
     * @param {Event} event
     */
    function onIdentifierChange(event) {
        /** @type {HTMLInputElement} */
        var target = event.target;

        if (isSweden()) {
            CheckoutModel.govId(target.value);
        } else if (isNorway()) {
            CheckoutModel.phone(target.value);
        }
    }

    /**
     * @param {RbsLib.Session.Response} response
     */
    function onSetSessionDone(response) {
        if (response.error.message !== '') {
            setError(response.error.message);
            customerTypeEls.each(function (i, el) {
                selectCustomerTypeRadio(el, CheckoutModel.isCompany());
            });
        } else {
            StorageLib.setIsCompany(!CheckoutModel.isCompany());
            CheckoutModel.isCompany(!CheckoutModel.isCompany());
            setInputLabel();
            setError('');
        }
    }

    /**
     * @param {MouseEvent} event
     */
    function onCustomerTypeChange(event) {
        /** @type {HTMLInputElement} */
        var target = event.target;
        var isCompany = isCompanyCustomer(target.value);

        disableInputs();

        SessionLib.setSessionData(isCompany)
            .done(onSetSessionDone)
            .always(enableInputs);
    }

    $(document).ready(function init() {
        var fetchedAddress = CheckoutConfig.getFetchedAddress();
        errorEl = $('#psrbsimplified-fetch-address-error');
        fetchBtnEl = $('#psrbsimplified-btn-fetch-address');
        identifierInputEl = $('#psrbsimplified-fetch-address-input');
        inputLabelEl = $('#psrbsimplified-fetch-address-input-label');
        customerTypeEls = $(
            'input[type="radio"][name="prsbsimplifiedCustomerType"]'
        );

        identifierInputEl.on('change', onIdentifierChange);
        identifierInputEl.val(CheckoutConfig.getGovId());

        if (CheckoutConfig.getGovId()) {
            renderResetButton();
        } else {
            renderFetchAddressButton();
        }

        renderFetchAddressButton();

        customerTypeEls.each(
            /**
             * @param {number} i
             * @param {HTMLInputElement} el
             */
            function (i, el) {
                el.checked = el.value === selectedCustomerType;
                $(el).on('click', onCustomerTypeChange);
            }
        );

        if (typeof StorageLib.getIsCompany() === 'boolean') {
            CheckoutModel.isCompany(StorageLib.getIsCompany());
        }

        setInputLabel();

        if (typeof fetchedAddress !== 'string') {
            CheckoutLib.applyAddress(fetchedAddress);
        }

        insertFetchAddressWidget();
    });
}(
    $,
    psrbsimplified.Lib.FetchAddress,
    psrbsimplified.Lib.CheckoutConfig,
    psrbsimplified.Lib.Credentials,
    psrbsimplified.Lib.Checkout,
    psrbsimplified.Lib.Storage,
    psrbsimplified.Lib.Session,
    psrbsimplified.Model.Checkout
));
