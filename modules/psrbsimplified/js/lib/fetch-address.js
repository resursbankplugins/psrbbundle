/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

/**
 * @type RbsLib.FetchAddress
 */
window.psrbsimplified.Lib.FetchAddress =
    /**
     * @param {jQuery} $
     * @param {RbsLib.CheckoutConfig} CheckoutConfig
     * @returns RbsLib.FetchAddress
     */
    (function (
        $,
        CheckoutConfig
    ) {
        'use strict';

        /**
         * @typedef {object} RbsLib.FetchAddress.Address
         * @property {string} firstname
         * @property {string} lastname
         * @property {string} city
         * @property {string} company
         * @property {string} country
         * @property {string} postcode
         * @property {string} street0
         * @property {string} street1
         * @property {string} telephone
         * @property {int} countryId
         */

        /**
         * @typedef {object} RbsLib.FetchAddress.Error
         * @property {string} message
         */

        /**
         * @typedef {object} RbsLib.FetchAddress.Call
         * @property {string} type
         * @property {string} url
         * @property {object} data
         * @property {string} data.identifier
         * @property {boolean} data.is_company
         * @property {boolean} data.ajax
         */

        /**
         * @typedef {object} RbsLib.FetchAddress.RemoveCall
         * @property {string} type
         * @property {string} url
         * @property {object} data
         * @property {boolean} data.ajax
         */

        /**
         * @typedef {object} RbsLib.FetchAddress.Response
         * @property {RbsLib.FetchAddress.Address} address
         * @property {RbsLib.FetchAddress.Error} error
         */

        /**
         * @constant
         * @readonly
         * @namespace RbsLib.FetchAddress
         */
        var EXPORT = {
            /**
             * Sends a request to the server that returns the address
             * information for the supplied SSN/Org. nr (Sweden), or phone
             * number (Norway).
             *
             * @param {string} identifier
             * @param {boolean} isCompany
             * @return {jQuery}
             */
            fetchAddress: function (identifier, isCompany) {
                return $.ajax(EXPORT.getFetchAddressCall(
                    identifier,
                    isCompany
                ));
            },

            /**
             * Produces a call object to make a request to fetch the address of
             * a given SSN/Org. nr (Sweden) or phone number (Norway).
             *
             * @param {string} identifier
             * @param {boolean} isCompany
             * @returns {RbsLib.FetchAddress.Call}
             */
            getFetchAddressCall: function (identifier, isCompany) {
                return {
                    type: 'POST',
                    dataType: 'json',
                    url: CheckoutConfig.getFetchAddressUrl(),
                    data: {
                        ajax: true,
                        identifier: identifier,
                        is_company: isCompany
                    }
                };
            },

            /**
             * Sends a request to the server that will remove any fetched
             * address from the session.
             *
             * @return {jQuery}
             */
            removeAddress: function () {
                return $.ajax(EXPORT.getRemoveAddressCall());
            },

            /**
             * Produces a call object to make a request to remove a fetched
             * address from the session.
             *
             * @returns {RbsLib.FetchAddress.RemoveCall}
             */
            getRemoveAddressCall: function () {
                return {
                    type: 'POST',
                    dataType: 'json',
                    url: CheckoutConfig.getRemoveAddressUrl(),
                    data: {
                        ajax: true
                    }
                };
            }
        };

        return Object.freeze(EXPORT);
    }(
        $,
        window.psrbsimplified.Lib.CheckoutConfig
    ));
