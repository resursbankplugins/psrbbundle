<?php
// se.php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{psrbsimplified}prestashop>fetchaddressfield_74680dc2a8bec47ca390bc6643874054'] = 'Privatperson';
$_MODULE['<{psrbsimplified}prestashop>fetchaddressfield_1c76cbfe21c6f44c1d1e59d54f3e4420'] = 'Företag';
$_MODULE['<{psrbsimplified}prestashop>fetchaddressfield_4fbf180e4354c7b3d7ea67451e81db99'] = 'Hämta adress';
$_MODULE['<{psrbsimplified}prestashop>psrbsimplified_9bda760340d72928e632a983350508ad'] = 'Telefonnummer är felaktigt eller saknas.';
$_MODULE['<{psrbsimplified}prestashop>psrbsimplified_4dfadb87040ca1c5d38ae5cd74e3710f'] = 'E-postadress är felaktig eller saknas.';
$_MODULE['<{psrbsimplified}prestashop>psrbsimplified_8718665f3b146191e85571c543d62512'] = 'Vi kunde inte hantera denna beställning. Vänligen försök igen eller kontakta vår support';
$_MODULE['<{psrbsimplified}prestashop>psrbsimplified_ee139d06928bbb94fcac8e80418ed11f'] = 'Betalning kunde inte slutföras. Kontakta kundtjänst för mer information.';
$_MODULE['<{psrbsimplified}prestashop>psrbsimplified_5d05c03ff627bbeed585ced31d30da3f'] = 'Kunde inte återskapa kundvagnen. Felet har loggats.';
$_MODULE['<{psrbsimplified}prestashop>psrbsimplified_f5435d1d8e776f9e8a5ce09c7a82e345'] = 'Vi kan inte fortsätta med din beställning då vi saknar information om den. Vänligen försök igen.';
$_MODULE['<{psrbsimplified}prestashop>psrbsimplified_2d719bfd501a938bb9116284335a63e3'] = 'Din beställning nekades av Resurs Bank. Vänligen försök igen eller kontakta vår kundtjänst';
$_MODULE['<{psrbsimplified}prestashop>psrbsimplified_730c4e150fce475ebfcac5cb29ce01b3'] = 'Personnummer';
$_MODULE['<{psrbsimplified}prestashop>psrbsimplified_1f8261d17452a959e013666c5df45e07'] = 'Telefonnummer';
$_MODULE['<{psrbsimplified}prestashop>psrbsimplified_d1457170f205323fa446500f864338e7'] = 'Mobilnummer';
$_MODULE['<{psrbsimplified}prestashop>psrbsimplified_8b5dd64ab8d0b8158906796b53a200e2'] = 'E-postadress';
$_MODULE['<{psrbsimplified}prestashop>psrbsimplified_a44217022190f5734b2f72ba1e4f8a79'] = 'Kortnummer';
$_MODULE['<{psrbsimplified}prestashop>psrbsimplified_0f65d94aad281e03ec316cbf6e0165ee'] = 'Ogiltigt person- eller organisationsnummer.';
$_MODULE['<{psrbsimplified}prestashop>psrbsimplified_6ccd95d308cda851ec3255a9eb91937f'] = 'Sökandes fullständiga namn';
$_MODULE['<{psrbsimplified}prestashop>psrbsimplified_bdfc2687859a97a17733902030f9a8ef'] = 'Sökandes telefonnummer';
$_MODULE['<{psrbsimplified}prestashop>psrbsimplified_04cc1a698a78455626e630dc2ab2aaa1'] = 'Sökandes mobilnummer';
$_MODULE['<{psrbsimplified}prestashop>psrbsimplified_b255adc379369903c7107d72d192871c'] = 'Ogiltigt telefonnummer.';
$_MODULE['<{psrbsimplified}prestashop>psrbsimplified_8bcd2d38ea40647b7e8a8419baa0fd60'] = 'Ogiltigt personnummer.';
$_MODULE['<{psrbsimplified}prestashop>psrbsimplified_459228e6ac14a65555cbc1794e661506'] = 'Ogiltigt organisationsnummer.';
$_MODULE['<{psrbsimplified}prestashop>psrbsimplified_ec98057e0247a8f557919219f0e97922'] = 'Organisationsnummer (XXXXXXXXXX)';
$_MODULE['<{psrbsimplified}prestashop>psrbsimplified_696dcf213b72df4962b341b3f354faab'] = 'Personnummer (YYYYMMDDXXXX)';
$_MODULE['<{psrbsimplified}prestashop>psrbsimplified_e44d34afa2de844e6e565bfe2afd334a'] = 'Inte du?';
$_MODULE['<{psrbsimplified}prestashop>psrbsimplified_4fbf180e4354c7b3d7ea67451e81db99'] = 'Hämta adress';
$_MODULE['<{psrbsimplified}prestashop>psrbsimplified_8d1b5f6c31c37a2f29b0c30b547e7e39'] = 'Kontakts personnummer';
$_MODULE['<{psrbsimplified}prestashop>psrbsimplified_d90409347142a589f0d906f47e328959'] = 'Sökandes personnummer';
$_MODULE['<{psrbsimplified}prestashop>psrbsimplified_3703dfb7ad4ae0c860987ebbe6a34fe3'] = 'Sökandes e-postadress';
$_MODULE['<{psrbsimplified}prestashop>psrbsimplified_92881a6240a9fd8f54a3f3056aacf92c'] = 'Kunde inte hantera beställning - kundvang är inte giltig eller modul är ej aktiverad.';

