# Resurs Bank - PrestaShop module - Simplified Flow

## Description

Simplified Flow API implementation for PrestaShop module.

---

## Prerequisites

* [PrestaShop 1.7.7+]

---

#### 1.0.0

* Initial release.
