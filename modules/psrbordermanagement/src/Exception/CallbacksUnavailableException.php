<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\OrderManagement\Exception;

use PrestaShopException;

/**
 * Indicates a problem with validating an incoming callback.
 */
class CallbacksUnavailableException extends PrestaShopException
{
}
