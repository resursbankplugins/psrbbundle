<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\OrderManagement\Exception;

use PrestaShopException;

/**
 * Indicates order status could not be resolved.
 */
class ResolveOrderStatusFailedException extends PrestaShopException
{
}
