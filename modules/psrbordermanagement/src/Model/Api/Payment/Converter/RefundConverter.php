<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\OrderManagement\Model\Api\Payment\Converter;

use Exception;
use Order;
use PrestaShop\PrestaShop\Adapter\Entity\Carrier;
use PrestaShop\PrestaShop\Adapter\Entity\OrderCartRule;
use PrestaShop\PrestaShop\Adapter\Entity\OrderDetail;
use Resursbank\Core\Model\Api\Payment\Converter\AbstractConverter;
use Resursbank\Core\Model\Api\Payment\Converter\Item\ProductItem;
use Resursbank\Core\Model\Api\Payment\Item as PaymentItem;

/**
 * Refund entity conversion for order refunds.
 */
class RefundConverter extends AbstractConverter
{
    /**
     * Convert supplied entity to a collection of PaymentItem instances. These
     * objects can later be mutated into a simple array the API can interpret.
     *
     * @param Order $order
     *
     * @return PaymentItem[]
     *
     * @throws Exception
     */
    public function convert(
        Order $order
    ): array {
        $carrier = new Carrier($order->id_carrier, $order->id_lang);
        $orderCartRules = $order->getCartRules();
        $discountData = [];

        /** @var OrderCartRule $orderCartRule */
        foreach ($orderCartRules as $orderCartRule) {
            if (!$orderCartRule instanceof OrderCartRule) {
                $orderCartRule = new OrderCartRule($orderCartRule['id_order_cart_rule']);
            }

            $value = (float) $orderCartRule->value;
            $vat = $value - (float) $orderCartRule->value_tax_excl;
            $name = (string) $orderCartRule->name;

            $discountData[] = $this->getDiscountData($name, $value, $vat);
        }

        return array_merge(
            array_merge(
                $this->getShippingData(
                    (string) $carrier->name,
                    (string) $carrier->name,
                    (float) $order->total_shipping_tax_incl,
                    (float) $order->carrier_tax_rate
                ),
                $discountData
            ),
            $this->getProductData($order)
        );
    }

    /**
     * Extract product information from Quote entity.
     *
     * @param Order $order
     *
     * @return PaymentItem[]
     *
     * @throws Exception
     */
    protected function getProductData(
        Order $order
    ): array {
        $result = [];

        if ($this->includeProductData($order)) {
            /** @var OrderDetail $orderDetail */
            foreach ($order->getOrderDetailList() as $orderDetail) {
                if (!$orderDetail instanceof OrderDetail) {
                    $orderDetail = new OrderDetail((int) $orderDetail['id_order_detail']);
                }
                $orderDetail->product_quantity -= $orderDetail->product_quantity_refunded;
                $item = new ProductItem($orderDetail);

                $result[] = $item->getItem();
            }
        }

        return $result;
    }

    /**
     * Whether to include product data in payment payload.
     *
     * @param Order $entity
     *
     * @return bool
     */
    public function includeProductData(
        Order $entity
    ): bool {
        $items = $entity->getOrderDetailList();

        return !empty($items);
    }
}
