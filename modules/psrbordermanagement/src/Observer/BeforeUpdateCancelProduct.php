<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\OrderManagement\Observer;

use Doctrine\ORM\ORMException;
use Exception;
use InvalidArgumentException;
use PrestaShop\PrestaShop\Core\Domain\Shop\ValueObject\ShopConstraint;
use function is_array;
use PrestaShop\PrestaShop\Adapter\Entity\Carrier;
use PrestaShop\PrestaShop\Adapter\Entity\Order;
use PrestaShop\PrestaShop\Adapter\Entity\OrderDetail;
use PrestaShop\PrestaShop\Core\Domain\Order\Payment\Exception\PaymentException;
use PrestaShop\PrestaShop\Core\Domain\Order\VoucherRefundType;
use PrestaShopBundle\Service\Routing\Router;
use PrestaShopDatabaseException;
use PrestaShopException;
use psrbordermanagement;
use Resursbank\Core\Api\Credentials;
use Resursbank\Core\Logger\LoggerInterface;
use Resursbank\Core\Model\Api\Payment\Converter\Item\ProductItem;
use Resursbank\Core\Model\Api\Payment\Item;
use Resursbank\OrderManagement\Config\Config;
use Resursbank\OrderManagement\Model\Api\Payment\Converter\RefundConverter;
use Resursbank\OrderManagement\Service\OrderManagement;

/**
 * Reflect partial refund to payment at Resurs Bank.
 */
class BeforeUpdateCancelProduct extends AbstractObserver
{
    /**
     * @var OrderManagement
     */
    private $orderManagement;

    /**
     * @var RefundConverter
     */
    private $refundConverter;

    /**
     * @param LoggerInterface $logger
     * @param Credentials $credentials
     * @param Config $config
     * @param Router $router
     * @param OrderManagement $orderManagement
     * @param RefundConverter $refundConverter
     */
    public function __construct(
        LoggerInterface $logger,
        Credentials $credentials,
        Config $config,
        Router $router,
        OrderManagement $orderManagement,
        RefundConverter $refundConverter
    ) {
        parent::__construct($logger, $credentials, $config, $router);

        $this->orderManagement = $orderManagement;
        $this->refundConverter = $refundConverter;
    }

    /**
     * $params = ['id' => orderId, 'form_data' => FormDataFromAdmin]
     *
     * @param psrbordermanagement $module
     * @param array $params
     *
     * @return void
     *
     * @throws PrestaShopDatabaseException
     * @throws PrestaShopException
     * @throws Exception
     */
    public function execute(psrbordermanagement $module, array $params): void
    {
        // Validate anonymous data in $params.
        $this->validateParams($params);
        $order = new Order($params['id']);
        if (!$this->config->isEnabled(new ShopConstraint(
            (int)$order->id_shop,
            (int)$order->id_shop_group)
        )) {
            return;
        }

        // Figure out if a cancellation, refund or return was made
        if (isset($params['route'])) {
            switch ($params['route']) {
                case 'admin_orders_cancellation':
                    $this->cancelProducts($order, $params['form_data']);
                    break;
                case 'admin_orders_standard_refund':
                case 'admin_orders_partial_refund':
                case 'admin_orders_return_product':
                    $this->refundProducts($order, $params['form_data']);
                    break;
            }
        }
    }

    /**
     * @throws Exception
     */
    private function refundProducts(Order $order, array $formData): void
    {
        // Resolve relevant data from params.
        [$refundedItems, $totalQty] = $this->getProductData($formData);
        $orderedQty = 0;
        /** @var OrderDetail $orderDetail */
        foreach ($order->getOrderDetailList() as $orderDetail) {
            $orderedQty += $orderDetail['product_quantity'] - $orderDetail['product_quantity_refunded'];
        }
        $refundShippingOrZero = (isset($formData['shipping']) &&
            $formData['shipping'] === true) ||
            (int)$order->total_shipping_tax_incl === 0;

        //$excludingVoucherRefund = false;
        if (isset($formData['voucher_refund_type'])
            && $formData['voucher_refund_type'] === VoucherRefundType::PRODUCT_PRICES_EXCLUDING_VOUCHER_REFUND
            && $order->total_discounts_tax_incl > 0
        ) {
            if ($discountData = $this->refundConverter->getDiscountData(
                'Discount',
                (float) $order->total_discounts_tax_incl,
                (float) $order->total_discounts_tax_incl - $order->total_discounts_tax_excl
            )) {
                //$excludingVoucherRefund = true;
                $refundedItems[] = $discountData;
            }
        }

        if ((isset($formData['shipping']) &&
                $formData['shipping'] === true) ||
            (isset($formData['shipping_amount']) &&
                (float) $formData['shipping_amount'] > 0)
        ) {
            $refundedItems = array_merge(
                $this->getShippingData($order, $formData),
                $refundedItems
            );
        }

        // refund whole order
        // Do we really want to do this, as the above code seems to handle this properly?
        /*
        if ((int)$orderedQty === (int)$totalQty &&
            $refundShippingOrZero &&
            !$excludingVoucherRefund
        ) {
            $this->orderManagement->refund($order);
            return;
        }
        */

        $this->orderManagement->refund($order, $refundedItems);
    }

    /**
     * @throws Exception
     */
    private function cancelProducts(Order $order, array $formData): void
    {
        // Resolve relevant data from params.
        [$canceledProducts, $totalQtyCanceled] = $this->getProductData($formData);
        $orderedQty = 0;
        /** @var OrderDetail $orderDetail */
        foreach ($order->getOrderDetailList() as $orderDetail) {
            $orderedQty += $orderDetail['product_quantity'];
        }

        // Cancel whole order if total
        if ((int)$orderedQty === (int)$totalQtyCanceled) {
            $this->orderManagement->cancel($order);
            return;
        }

        $this->orderManagement->cancel($order, $canceledProducts);
    }

    /**
     * @param array $params
     *
     * @return void
     */
    private function validateParams(array $params): void
    {
        if (!isset($params['id']) || (int) $params['id'] === 0) {
            throw new InvalidArgumentException('No order id supplied.');
        }

        if (!isset($params['form_data']) || !is_array($params['form_data'])) {
            throw new InvalidArgumentException('No form data.');
        }
    }

    /**
     * @param float $priceExclVat
     * @param float $priceInclVat
     *
     * @return float
     */
    public function getVatPct(float $priceExclVat, float $priceInclVat): float
    {
        // There is no reliable way to get the VAT percentage, so we have
        // to calculate it ourselves
        return ($priceInclVat - $priceExclVat) / $priceExclVat;
    }

    /**
     * @param array $formData
     *
     * @return array<Item>
     *
     * @throws Exception
     */
    private function getProductData(array $formData): array
    {
        $refundedProducts = [];
        $totalQty = 0;

        if (!isset($formData['products']) || !is_array($formData['products'])) {
            throw new InvalidArgumentException('No product data.');
        }

        // @todo We need to validate all of the anonymous properties and objects.
        foreach ($formData['products'] as $product) {
            $orderDetailId = $product->getOrderDetailId();
            if (!empty($formData['quantity_' . $orderDetailId]) || !empty((float) $formData['amount_' . $orderDetailId])) {
                $orderDetail = new OrderDetail($orderDetailId);
                $qty = $formData['quantity_' . $orderDetailId];
                $amount = $formData['amount_' . $orderDetailId];
                $selected = $formData['selected_' . $orderDetailId];
                // If the amount refunded is not the exact amount of the unit price
                // Or if it's selected meaning a full refund
                if (!$selected && $orderDetail->total_price_tax_incl !== $amount / $qty) {
                    $vatPtc = $this->getVatPct(
                        (float) $orderDetail->unit_price_tax_excl,
                        (float) $orderDetail->unit_price_tax_incl
                    );
                    $orderDetail->product_quantity = $qty;
                    $orderDetail->unit_price_tax_incl = $amount / $qty;
                    $orderDetail->unit_price_tax_excl = $amount / (1 + $vatPtc) / $qty;
                } elseif ($selected) {
                    $orderDetail->product_quantity = $qty;
                }
                $totalQty += $qty;
                $item = new ProductItem($orderDetail);

                $refundedProducts[] = $item->getItem();
            }
        }

        return [$refundedProducts, (int)$totalQty];
    }

    /**
     * @param Order $order
     * @param array $formData
     * @return array|Item[]
     *
     * @throws Exception
     */
    private function getShippingData(Order $order, array $formData): array
    {
        $carrier = new Carrier($order->id_carrier, $order->id_lang);
        $shippingAmount = isset($formData['shipping_amount']) && $formData['shipping_amount'] > 0 ? $formData['shipping_amount'] : $order->total_shipping_tax_incl;
        return $this->refundConverter->getShippingData(
            (string) $carrier->name,
            (string) $carrier->name,
            (float) $shippingAmount,
            (float) $order->carrier_tax_rate
        );
    }
}
