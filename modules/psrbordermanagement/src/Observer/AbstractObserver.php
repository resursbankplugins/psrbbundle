<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\OrderManagement\Observer;

use PrestaShopBundle\Service\Routing\Router;
use psrbordermanagement;
use Resursbank\Core\Api\Credentials;
use Resursbank\Core\Logger\LoggerInterface;
use Resursbank\OrderManagement\Config\Config;

/**
 * Implementation rules and general business logic for AfterShop integration.
 */
abstract class AbstractObserver
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var Credentials
     */
    protected $credentials;

    /**
     * @var Config
     */
    protected $config;

    /**
     * @var Router
     */
    protected $router;

    /**
     * @param LoggerInterface $logger
     * @param Credentials $credentials
     * @param Config $config
     * @param Router $router
     */
    public function __construct(
        LoggerInterface $logger,
        Credentials $credentials,
        Config $config,
        Router $router
    ) {
        $this->logger = $logger;
        $this->credentials = $credentials;
        $this->config = $config;
        $this->router = $router;
    }

    /**
     * @param psrbordermanagement $module
     * @param array $params
     * @return void
     */
    abstract public function execute(
        psrbordermanagement $module,
        array $params
    ): void;
}
