<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\OrderManagement\Config\Form;

use Exception;
use PrestaShop\PrestaShop\Core\Form\FormDataProviderInterface;
use Resursbank\Core\Logger\Logger;
use Resursbank\Core\Logger\LoggerInterface;
use Resursbank\OrderManagement\Config\Config;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Configuration page data provider.
 */
class DataProvider implements FormDataProviderInterface
{
    /**
     * @var Config
     */
    private $config;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var \psrbordermanagement
     */
    private $module;

    /**
     * @param Config $config
     * @param Logger $logger
     * @param TranslatorInterface $translator
     */
    public function __construct(
        Config $config,
        LoggerInterface $logger,
        TranslatorInterface $translator
    ) {
        $this->config = $config;
        $this->logger = $logger;
        $this->translator = $translator;
        $this->module = \Module::getInstanceByName('psrbordermanagement');
    }

    /**
     * @inheridoc
     *
     * @return array
     */
    public function getData(): array
    {
        return [
            'general' => [
                'enabled' => $this->config->isEnabled(),
            ],
            'callback' => [
                'last_triggered_at' => $this->config->getCallbackLastTriggeredAt(),
                'last_received_at' => $this->config->getCallbackReceivedAt(),
            ],
            'state_mapping' => [
                'pending' => $this->config->getPendingState(),
                'processing' => $this->config->getProcessingState(),
                'annulled' => $this->config->getAnnulledState(),
                'completed' => $this->config->getCompletedState(),
                'credited' => $this->config->getCreditedState(),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     *
     * @todo The parent throws a specific type of Exception which we may wish to utilize, not sure if they are rendered in a specific way.
     */
    public function setData(
        array $data
    ): array {
        return array_filter([
            $this->setEnabled($data),
            $this->setPendingState($data),
            $this->setProcessingState($data),
            $this->setAnnulledState($data),
            $this->setCompletedState($data),
            $this->setCreditedState($data),
        ]);
    }

    /**
     * @param array $data
     *
     * @return string
     */
    public function setEnabled(
        array $data
    ): string {
        $error = '';

        try {
            if (isset($data['general']['enabled'])) {
                $this->config->setEnabled($data['general']['enabled']);
            }
        } catch (Exception $e) {
            $this->logger->exception($e);

            $error = $this->module->l('Failed to update enabled state.');
        }

        return $error;
    }

    /**
     * @param array $data
     *
     * @return string
     */
    private function setPendingState(
        array $data
    ): string {
        $error = '';

        try {
            if (isset($data['state_mapping']['pending'])) {
                $this->config->setPendingState((int) $data['state_mapping']['pending']);
            }
        } catch (Exception $e) {
            $this->logger->exception($e);

            $error = $this->module->l('Failed to update pending state mapping.');
        }

        return $error;
    }

    /**
     * @param array $data
     *
     * @return string
     */
    private function setProcessingState(
        array $data
    ): string {
        $error = '';

        try {
            if (isset($data['state_mapping']['processing'])) {
                $this->config->setProcessingState((int) $data['state_mapping']['processing']);
            }
        } catch (Exception $e) {
            $this->logger->exception($e);

            $error = $this->module->l('Failed to update processing state mapping.');
        }

        return $error;
    }

    /**
     * @param array $data
     *
     * @return string
     */
    private function setAnnulledState(
        array $data
    ): string {
        $error = '';

        try {
            if (isset($data['state_mapping']['annulled'])) {
                $this->config->setAnnulledState((int) $data['state_mapping']['annulled']);
            }
        } catch (Exception $e) {
            $this->logger->exception($e);

            $error = $this->module->l('Failed to update annulled state mapping.');
        }

        return $error;
    }

    /**
     * @param array $data
     *
     * @return string
     */
    private function setCompletedState(
        array $data
    ): string {
        $error = '';

        try {
            if (isset($data['state_mapping']['completed'])) {
                $this->config->setCompletedState((int) $data['state_mapping']['completed']);
            }
        } catch (Exception $e) {
            $this->logger->exception($e);

            $error = $this->module->l('Failed to update completed state mapping.');
        }

        return $error;
    }

    /**
     * @param array $data
     *
     * @return string
     */
    private function setCreditedState(
        array $data
    ): string {
        $error = '';

        try {
            if (isset($data['state_mapping']['credited'])) {
                $this->config->setCreditedState((int) $data['state_mapping']['credited']);
            }
        } catch (Exception $e) {
            $this->logger->exception($e);

            $error = $this->module->l('Failed to update credited state mapping.');
        }

        return $error;
    }
}
