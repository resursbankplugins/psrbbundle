<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\OrderManagement\Repository;

use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\ExpressionBuilder;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\ORMException;
use Exception;
use JsonException;
use Resursbank\OrderManagement\Entity\ResursbankPaymentHistory;

/**
 * Repository implementation of the Resurs Bank Payment History table.
 */
class ResursbankPaymentHistoryRepository extends EntityRepository
{
    /**
     * @param int $orderId
     *
     * @return Collection|null
     *
     * @throws Exception
     */
    public function getList(int $orderId): Collection
    {
        return $this->matching(
            Criteria::create()
                ->where($this->getExp()->eq('orderId', $orderId))
                ->orderBy(['createdAt' => 'desc'])
        );
    }

    /**
     * @return ExpressionBuilder
     *
     * @throws Exception
     */
    private function getExp(): ExpressionBuilder
    {
        $exp = Criteria::expr();

        if (!$exp instanceof ExpressionBuilder) {
            throw new Exception('Failed to generate criteria.');
        }

        return $exp;
    }

    /**
     * @param int $orderId
     * @param string $event
     * @param string $user
     * @param string|null $stateFrom
     * @param string|null $stateTo
     * @param array|null $extra
     *
     * @return ResursbankPaymentHistory
     *
     * @throws ORMException
     * @throws JsonException
     * @noinspection PhpTooManyParametersInspection
     */
    public function create(
        int $orderId,
        string $event,
        string $user = ResursbankPaymentHistory::USER_RESURS_BANK,
        ?string $stateFrom = null,
        ?string $stateTo = null,
        ?array $extra = null
    ): ResursbankPaymentHistory {
        $paymentHistory = new ResursbankPaymentHistory();
        $paymentHistory
            ->setOrderId($orderId)
            ->setEvent($event)
            ->setUser($user)
            ->setExtra($extra ?? [])
            ->setStateFrom($stateFrom)
            ->setStateTo($stateTo);
        $this->getEntityManager()->persist($paymentHistory);

        $this->getEntityManager()->flush();

        return $paymentHistory;
    }
}
