<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\OrderManagement\Service;

use Exception;
use PrestaShop\PrestaShop\Adapter\Entity\Link;
use psrbordermanagement;
use Resursbank\Core\Api\Connection;
use Resursbank\Core\Api\Credentials;
use Resursbank\Core\Logger\LoggerInterface;
use function constant;

/**
 * This class implements business logic to register, fetch and test callbacks.
 * Please note it's not designed to accept or process incoming callbacks, refer
 * to the module controllers for that implementation.
 */
class Callback
{
    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var Credentials
     */
    private $credentials;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var string
     */
    private $secret;

    /**
     * @param Connection $connection
     * @param Credentials $credentials
     * @param LoggerInterface $logger
     * @param string $secret
     */
    public function __construct(
        Connection $connection,
        Credentials $credentials,
        LoggerInterface $logger,
        string $secret
    ) {
        $this->connection = $connection;
        $this->credentials = $credentials;
        $this->logger = $logger;
        $this->secret = $secret;
    }

    /**
     * Register all callback methods.
     *
     * @return self
     *
     * @throws Exception
     */
    public function register(): self
    {
        $connection = $this->connection->getConnection(
            $this->credentials->get()
        );

        // Callback types.
        $types = ['unfreeze', 'booked', 'update', 'test'];

        // Unregister annulment, automatic_fraud_control and finalization.
        $connection->unregisterEventCallback(14, true);

        foreach ($types as $type) {
            $connection->setRegisterCallback(
                constant(
                    'Resursbank\Ecommerce\Types\Callback::' .
                    strtoupper($type)
                ),
                $this->urlCallbackTemplate($type),
                ['digestSalt' => $this->secret]
            );
        }

        return $this;
    }

    /**
     * Fetch registered callbacks.
     *
     * @return array
     */
    public function fetch(): array
    {
        $result = [];

        try {
            $credentials = $this->credentials->get();

            $result = $this->connection
                ->getConnection($credentials)
                ->getCallBacksByRest();
        } catch (Exception $e) {
            $this->logger->exception($e);
        }

        return $result;
    }

    /**
     * Trigger the test-callback.
     *
     * @return void
     *
     * @throws Exception
     */
    public function test(): void
    {
        $connection = $this->connection->getConnection(
            $this->credentials->get()
        );

        // NOTE: The three 's','a','d' values exist because the API expects five
        // values.
        $connection->triggerCallback([
            'i',
            's',
            's',
            'a',
            'd',
        ]);

        sleep(10);
    }

    /**
     * Retrieve callback URL template.
     *
     * @param string $type
     *
     * @return string
     */
    private function urlCallbackTemplate(
        string $type
    ): string {
        if ($type === 'test') {
            $params = [
                'p1' => 'a',
                'p2' => 'b',
                'p3' => 'c',
                'p4' => 'd',
                'p5' => 'e',
            ];
        } else {
            $params = [
                'paymentId' => '{paymentId}',
                'digest' => '{digest}',
            ];
        }
        $link = new Link();

        return $this->getPartialDecodedUrl(
            $link->getModuleLink(
                'psrbordermanagement',
                $type,
                $params,
                true
            )
        );
    }

    /**
     * Convert special characters in urlencoded string to Resurs compatible data.
     *
     * @param $uriString
     * @return string
     * @since 1.0.0
     */
    private function getPartialDecodedUrl($uriString): string
    {
        $replacer = [
            '%25' => '&',
            '%7B' => '{',
            '%7D' => '}',
        ];
        foreach ($replacer as $fromEncode => $toCharacter) {
            $uriString = str_replace($fromEncode, $toCharacter, $uriString);
        }
        return $uriString;
    }
}
