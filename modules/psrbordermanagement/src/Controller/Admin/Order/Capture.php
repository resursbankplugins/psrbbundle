<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\OrderManagement\Controller\Admin\Order;

use Exception;
use PrestaShop\PrestaShop\Adapter\Entity\Order;
use PrestaShopBundle\Controller\Admin\FrameworkBundleAdminController;
use Resursbank\Core\Logger\LoggerInterface;
use Resursbank\OrderManagement\Service\OrderManagement;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Capture a specific order at Resurs Bank
 */
class Capture extends FrameworkBundleAdminController
{
    /**
     * @var OrderManagement
     */
    private $orderManagement;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param OrderManagement $orderManagement
     * @param LoggerInterface $logger
     */
    public function __construct(
        OrderManagement $orderManagement,
        LoggerInterface $logger
    ) {
        parent::__construct();
        $this->orderManagement = $orderManagement;
        $this->logger = $logger;
    }

    /**
     * @param string $orderId
     *
     * @return RedirectResponse
     */
    public function execute(string $orderId): RedirectResponse
    {
        try {
            $order = new Order($orderId);
            $this->orderManagement->capture($order);
            $this->addFlash(
                'success',
                'Order successfully captured'
            );
        } catch (Exception $e) {
            $this->logger->exception($e);

            $this->addFlash(
                'error',
                'Unable to capture order.'
            );
        }

        return $this->redirectToRoute('admin_orders_view', [
            'orderId' => $orderId,
        ]);
    }
}
