<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\OrderManagement\Controller\Admin\Config;

use Exception;
use PrestaShopBundle\Controller\Admin\FrameworkBundleAdminController;
use Resursbank\Core\Logger\LoggerInterface;
use Resursbank\OrderManagement\Config\Config;
use Resursbank\OrderManagement\Service\Callback;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Executes API calls to register callback URLs at Resurs Bank.
 */
class CallbackRegistration extends FrameworkBundleAdminController
{
    /**
     * @var callback
     */
    private $callback;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var Config
     */
    private $config;

    /**
     * @var \psrbordermanagement
     */
    private $module;

    /**
     * @param callback $callback
     * @param LoggerInterface $logger
     */
    public function __construct(
        Callback $callback,
        LoggerInterface $logger,
        Config $config
    ) {
        parent::__construct();
        $this->callback = $callback;
        $this->logger = $logger;
        $this->config = $config;
        $this->module = \Module::getInstanceByName('psrbordermanagement');
    }

    /**
     * @return RedirectResponse
     */
    public function execute(): RedirectResponse
    {
        try {
            $this->callback->register();
            $this->config->setEnabled('1');
            $this->addFlash(
                'success',
                $this->module->l('Successful callback registration.')
            );
        } catch (Exception $e) {
            $this->logger->error('Callback URLs failed to register.');
            $this->addFlash(
                'error',
                $this->module->l('Callback URLs failed to register.' . $e->getMessage())
            );
        }

        return $this->redirectToRoute('resursbank_ordermanagement_admin_config');
    }
}
