<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\OrderManagement\Controller\Admin\Config;

use Exception;
use PrestaShopBundle\Controller\Admin\FrameworkBundleAdminController;
use Resursbank\Core\Logger\LoggerInterface;
use Resursbank\OrderManagement\Config\Config;
use Resursbank\OrderManagement\Service\Callback;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Integration of TEST callback from Resurs Bank.
 */
class CallbackTest extends FrameworkBundleAdminController
{
    /**
     * @var callback
     */
    private $callback;

    /**
     * @var Config
     */
    private $config;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var \psrbordermanagement
     */
    private $module;

    /**
     * @param callback $callback
     * @param Config $config
     * @param LoggerInterface $logger
     */
    public function __construct(
        Callback $callback,
        Config $config,
        LoggerInterface $logger
    ) {
        parent::__construct();
        $this->callback = $callback;
        $this->config = $config;
        $this->logger = $logger;
        $this->module = \Module::getInstanceByName('psrbordermanagement');
    }

    /**
     * @return RedirectResponse
     */
    public function execute(): RedirectResponse
    {
        try {
            $this->config->setCallbackLastTriggeredAt();
            $this->callback->test();
            $this->addFlash(
                'success',
                $this->module->l('Test callback was sent.')
            );
        } catch (Exception $e) {
            $this->addFlash('error', 'Test callback could not be triggered.');
            $this->logger->exception($e);
        }

        return $this->redirectToRoute('resursbank_ordermanagement_admin_config');
    }
}
