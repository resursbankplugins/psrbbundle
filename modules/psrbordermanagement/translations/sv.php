<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{psrbcore}prestashop>psrbcore_61949ca697764a9d96ac5fb304d0ce98'] = 'Resurs Bank Betalning';
$_MODULE['<{psrbcore}prestashop>psrbcore_b2f40690858b404ed10e62bdf422c704'] = 'Summa';
$_MODULE['<{psrbcore}prestashop>psrbcore_80d2677cf518f4d04320042f4ea6c146'] = 'Gräns';
$_MODULE['<{psrbcore}prestashop>psrbcore_68abcc3d0869f8a52c2b286b3c511dd3'] = 'Fryst';
$_MODULE['<{psrbcore}prestashop>psrbcore_93cba07454f06a4a960172bbd6e2a435'] = 'Ja';
$_MODULE['<{psrbcore}prestashop>psrbcore_bafd7322c6e97d25b6299b5d6fe8920b'] = 'Nej';
$_MODULE['<{psrbcore}prestashop>psrbcore_b130f12d0f2e4cc10edbe80fe31738dc'] = 'Bedrägeri';
$_MODULE['<{psrbcore}prestashop>psrbcore_707436a5aa13b82a4d777f64c717a625'] = 'Betalningsmetod';
$_MODULE['<{psrbcore}prestashop>psrbcore_49ee3087348e8d44e1feda1917443987'] = 'Namn';
$_MODULE['<{psrbcore}prestashop>psrbcore_dd7bf230fde8d4836917806aff6a6b27'] = 'Adress';
$_MODULE['<{psrbcore}prestashop>psrbcore_b2a1e8c4e743228e0a3719e3abc4cc1e'] = 'Telefon';
$_MODULE['<{psrbcore}prestashop>psrbcore_ce8ae9da5b7cd6c3df2929543a9af92d'] = 'E-post';
$_MODULE['<{psrbcore}prestashop>psrbcore_91a1bd08a417cbd1ce9ee50d8c67bab6'] = 'Betalningshistorik';
$_MODULE['<{psrbcore}prestashop>psrbcore_13a44a2e6523d601725d91712802583f'] = 'Test-callback skickades.';
$_MODULE['<{psrbcore}prestashop>psrbcore_88192be3d504227bb4a2d3eb8c9fb32a'] = 'Callback-registrering lyckades.';
$_MODULE['<{psrbcore}prestashop>psrbcore_9d37b88fa30457c6f30e45100c9faf65'] = 'Resurs Bank - Orderhantering';
$_MODULE['<{psrbcore}prestashop>psrbcore_df3ef92c5932b13bda0edcb00ce89fe3'] = 'Vänligen notera att ovanstående callbacks kan vara inkompatibla med Resurs Bank som kräver dem i https-format.';
$_MODULE['<{psrbcore}prestashop>psrbcore_b4a34d3f58b039e7685c2e39b5413757'] = 'Uppdatering lyckades.';
$_MODULE['<{psrbcore}prestashop>psrbcore_66ecb141a3a09bf4fbdc88ad093d6a0a'] = 'Misslyckades med uppdatering av krediterad-state-mappning.';
$_MODULE['<{psrbcore}prestashop>psrbcore_2c9b4aab762f1cfd7b381d950a2ebae0'] = 'Misslyckades med uppdatering av komplett-state-mappning.';
$_MODULE['<{psrbcore}prestashop>psrbcore_2f88bb671a5adaaa4c6f796316159a2f'] = 'Misslyckades med uppdatering av annullerad-state-mappning.';
$_MODULE['<{psrbcore}prestashop>psrbcore_9ac5e90435071059d77dab2903de0ff8'] = 'Misslyckades med uppdatering av behandlas-state-mappning.';
$_MODULE['<{psrbcore}prestashop>psrbcore_b40e90a1bc06b0a097f639b70b3527db'] = 'Misslyckades med uppdatering av väntande-state-mappning.';
$_MODULE['<{psrbcore}prestashop>psrbcore_6f36d3862f31995d464f58f0e204ccba'] = 'Misslyckades med att uppdatera aktiverad-state.';

