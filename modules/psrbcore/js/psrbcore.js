/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

/**
 * @namespace RbcModel
 */

/**
 * @namespace RbcView
 */

/**
 * @namespace RbcLib
 */

/**
 * Global variable.
 *
 * @typedef {object} psrbcore
 * @property {psrbcore.View} View
 * @property {psrbcore.Model} Model
 * @property {psrbcore.Lib} Lib
 * @property {string} getCostOfPurchaseUrl
 */

/**
 * @typedef {object} psrbcore.View
 * @property {RbcView.ReadMoreFn} ReadMore
 */

/**
 * @typedef {object} psrbcore.Model
 * @property {RbcModel.ReadMore} ReadMore
 */

/**
 * @typedef {object} psrbcore.Lib
 * @property {RbcLib.Config} Config
 */

if (window.psrbcore === undefined) {
    window.psrbcore = {};
}

if (window.psrbcore.Lib === undefined) {
    window.psrbcore.Lib = {};
}

if (window.psrbcore.Model === undefined) {
    window.psrbcore.Model = {};
}

if (window.psrbcore.View === undefined) {
    window.psrbcore.View = {};
}
