<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Core\Controller\Admin\Config;

use function count;
use Exception;
use function is_array;
use PrestaShopBundle\Controller\Admin\FrameworkBundleAdminController;
use Resursbank\Core\Config\Form\DataProvider;
use Resursbank\Core\Exception\ConfigException;
use Resursbank\Core\Logger\LoggerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Sync data from Resurs Bank API to local database.
 */
class Save extends FrameworkBundleAdminController
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var DataProvider
     */
    private $dataProvider;

    /**
     * @var \psrbcore
     */
    private $module;

    /**
     * @param LoggerInterface $logger
     * @param DataProvider $dataProvider
     */
    public function __construct(
        LoggerInterface $logger,
        DataProvider $dataProvider
    ) {
        parent::__construct();

        $this->logger = $logger;
        $this->dataProvider = $dataProvider;
        $this->module = \Module::getInstanceByName('psrbcore');
    }

    /**
     * Fetch payment methods from API and sync them to database.
     *
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function execute(
        Request $request
    ): RedirectResponse {
        try {
            $data = $request->get('form');

            if (!is_array($data)) {
                throw new ConfigException('Invalid or missing form data.');
            }

            $errors = $this->dataProvider->setData($data);

            if (count($errors) > 0) {
                $this->flashErrors($errors);
            } else {
                $this->addFlash(
                    'success',
                    $this->module->l('Successful update.')
                );
            }
        } catch (Exception $e) {
            // @todo Not sure if this is really safe?
            $this->addFlash('error', $e->getMessage());
            $this->logger->exception($e);
        }

        return $this->redirectToRoute('resursbank_admin_config');
    }
}
