<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Core\Model\Api;

use InvalidArgumentException;

/**
 * Represents payment method fetched through the API. This data is incompatible
 * with the platform payment standard and has to be converted before being used.
 */
class Method
{
    /**
     * Private customer type definition.
     */
    public const CUSTOMER_TYPE_PRIVATE = 'NATURAL';

    /**
     * Company customer type definition.
     */
    public const CUSTOMER_TYPE_COMPANY = 'LEGAL';

    /**
     * ID of the method.
     *
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $description;

    /**
     * @var float
     */
    private $minLimit;

    /**
     * @var float
     */
    private $maxLimit;

    /**
     * @var string
     */
    private $type;

    /**
     * @var array<string>
     */
    private $customerType;

    /**
     * @var string
     */
    private $specificType;

    /**
     * @param string $id
     * @param string $description
     * @param float $minLimit
     * @param float $maxLimit
     * @param string $type
     * @param array<string> $customerType
     * @param string $specificType
     */
    public function __construct(
        string $id = '',
        string $description = '',
        float $minLimit = 0.0,
        float $maxLimit = 0.0,
        string $type = '',
        array $customerType = [],
        string $specificType = ''
    ) {
        $this->setId($id)
            ->setDescription($description)
            ->setMinLimit($minLimit)
            ->setMaxLimit($maxLimit)
            ->setType($type)
            ->setCustomerType($customerType)
            ->setSpecificType($specificType);
    }

    /**
     * @see Method::$id
     *
     * @param string $value
     *
     * @return self
     *
     * @throws InvalidArgumentException
     */
    public function setId(
        string $value
    ): self {
        $this->validateId($value);

        $this->id = $value;

        return $this;
    }

    /**
     * @see Method::$id
     *
     * @param string $value
     *
     * @throws InvalidArgumentException
     */
    public function validateId(
        string $value
    ): void {
        if ($value === '') {
            throw new InvalidArgumentException('ID must not be empty.');
        }
    }

    /**
     * @see Method::$id
     *
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @see Method::$description
     *
     * @param string $value
     *
     * @return self
     *
     * @throws InvalidArgumentException
     */
    public function setDescription(
        string $value
    ): self {
        $this->validateDescription($value);

        $this->description = $value;

        return $this;
    }

    /**
     * @see Method::$description
     *
     * @param string $value
     *
     * @throws InvalidArgumentException
     */
    public function validateDescription(
        string $value
    ): void {
        if ($value === '') {
            throw new InvalidArgumentException(
                'Description must not be empty.'
            );
        }
    }

    /**
     * @see Method::$description
     *
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @see Method::$minLimit
     *
     * @param float $value
     *
     * @return self
     *
     * @throws InvalidArgumentException
     */
    public function setMinLimit(
        float $value
    ): self {
        $this->validateMinLimit($value);

        $this->minLimit = $value;

        return $this;
    }

    /**
     * @see Method::$minLimit
     *
     * @param float $value
     *
     * @throws InvalidArgumentException
     */
    public function validateMinLimit(
        float $value
    ): void {
        if ($value < 0) {
            throw new InvalidArgumentException('Min limit must be positive.');
        }
    }

    /**
     * @see Method::$minLimit
     *
     * @return float
     */
    public function getMinLimit(): float
    {
        return $this->minLimit;
    }

    /**
     * @see Method::$maxLimit
     *
     * @param float $value
     *
     * @return self
     *
     * @throws InvalidArgumentException
     */
    public function setMaxLimit(
        float $value
    ): self {
        $this->validateMaxLimit($value);

        $this->maxLimit = $value;

        return $this;
    }

    /**
     * @see Method::$maxLimit
     *
     * @param float $value
     *
     * @throws InvalidArgumentException
     */
    public function validateMaxLimit(
        float $value
    ): void {
        if ($value < 0) {
            throw new InvalidArgumentException('Max limit must be positive.');
        }
    }

    /**
     * @see Method::$maxLimit
     *
     * @return float
     */
    public function getMaxLimit(): float
    {
        return $this->maxLimit;
    }

    /**
     * @see Method::$type
     *
     * @param string $value
     *
     * @return self
     *
     * @throws InvalidArgumentException
     */
    public function setType(
        string $value
    ): self {
        $this->validateType($value);

        $this->type = $value;

        return $this;
    }

    /**
     * @see Method::$type
     *
     * @param string $value
     *
     * @throws InvalidArgumentException
     */
    public function validateType(
        string $value
    ): void {
        if ($value === '') {
            throw new InvalidArgumentException(
                'Type must not be empty.'
            );
        }
    }

    /**
     * @see Method::$type
     *
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @see Method::$customerType
     *
     * @param array<string> $value
     *
     * @return self
     *
     * @throws InvalidArgumentException
     */
    public function setCustomerType(
        array $value
    ): self {
        $this->validateCustomerType($value);

        $this->customerType = $value;

        return $this;
    }

    /**
     * @see Method::$customerType
     *
     * @param array<string> $value
     *
     * @throws InvalidArgumentException
     */
    public function validateCustomerType(
        array $value
    ): void {
        foreach ($value as $v) {
            if (
                $v !== self::CUSTOMER_TYPE_PRIVATE &&
                $v !== self::CUSTOMER_TYPE_COMPANY
            ) {
                throw new InvalidArgumentException(
                    sprintf(
                        'Customer type my be either %s or %s.',
                        self::CUSTOMER_TYPE_PRIVATE,
                        self::CUSTOMER_TYPE_COMPANY
                    )
                );
            }
        }
    }

    /**
     * @see Method::$customerType
     *
     * @return array<string>
     */
    public function getCustomerType(): array
    {
        return $this->customerType;
    }

    /**
     * @see Method::$specificType
     *
     * @param string $value
     *
     * @return self
     *
     * @throws InvalidArgumentException
     */
    public function setSpecificType(
        string $value
    ): self {
        $this->validateSpecificType($value);

        $this->specificType = $value;

        return $this;
    }

    /**
     * @see Method::$specificType
     *
     * @param string $value
     *
     * @throws InvalidArgumentException
     */
    public function validateSpecificType(
        string $value
    ): void {
        if ($value === '') {
            throw new InvalidArgumentException(
                'Specific type must not be empty.'
            );
        }
    }

    /**
     * @see Method::$specificType
     *
     * @return string
     */
    public function getSpecificType(): string
    {
        return $this->specificType;
    }
}
