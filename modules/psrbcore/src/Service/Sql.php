<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Core\Service;

use Db;
use Exception;
use Resursbank\Core\Exception\InvalidDataException;
use Resursbank\Core\Exception\MissingPathException;

/**
 * Methods to handle SQL queries.
 */
class Sql
{
    /**
     * @throws MissingPathException
     * @throws InvalidDataException
     * @throws Exception
     */
    public static function exec(
        string $module,
        string $file
    ): void {
        foreach (self::getQuery($module, $file) as $query) {
            if (!Db::getInstance()->execute(trim($query))) {
                throw new Exception('Failed to execute SQL query.');
            }
        }
    }

    /**
     * @throws MissingPathException
     * @throws InvalidDataException
     */
    public static function getQuery(
        string $module,
        string $file
    ): array {
        $file = Filesystem::getSql($module, $file);

        $query = file_get_contents($file);

        if (!is_string($query) || $query === '') {
            throw new InvalidDataException('Failed to read install.sql');
        }

        $query = str_replace(
            ['PREFIX_', 'ENGINE_TYPE'],
            [_DB_PREFIX_, _MYSQL_ENGINE_],
            $query
        );

        return preg_split(
            "/;\s*[\r\n]+/",
            trim($query)
        );
    }
}
