<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Core\Api;

use Exception;
use Module;
use Order;
use PrestaShop\PrestaShop\Core\Domain\Shop\Exception\ShopException;
use PrestaShop\PrestaShop\Core\Domain\Shop\ValueObject\ShopConstraint;
use Resursbank\Core\Api\Credentials as CredentialsApi;
use Resursbank\Core\Api\Credentials as CredentialsService;
use Resursbank\Core\Exception\InvalidDataException;
use Resursbank\Core\Model\Api\Address;
use Resursbank\Core\Model\Api\Credentials;
use Resursbank\Core\Model\Api\Customer;
use Resursbank\Core\Model\Api\Payment as PaymentModel;
use Resursbank\Core\ModuleInterface;
use Resursbank\Core\Service\Order as OrderService;
use Resursbank\RBEcomPHP\ResursBank;
use ResursException;
use stdClass;
use TorneLIB\Exception\ExceptionHandler;

/**
 * Implement methods to handle interact with the API.
 */
class Connection
{
    /**
     * @var CredentialsApi
     */
    private $credentials;

    /**
     * @var OrderService
     */
    private $orderService;

    /**
     * @param CredentialsApi $credentials
     * @param OrderService $orderService
     */
    public function __construct(
        CredentialsService $credentials,
        OrderService $orderService
    ) {
        $this->credentials = $credentials;
        $this->orderService = $orderService;
    }

    /**
     * @param Credentials $credentials
     *
     * @return ResursBank
     *
     * @throws Exception
     */
    public function getConnection(
        Credentials $credentials
    ): ResursBank {
        // Establish API connection.
        $connection = new ResursBank(
            $credentials->getUsername(),
            $credentials->getPassword(),
            $credentials->getEnvironment()
        );

        // Enable WSDL cache to suppress redundant API calls.
        $connection->setWsdlCache(true);

        // Enable usage of PSP methods (in the latest releases of ecom, this setting is deprecated and
        // all payment methods are returned regardless of the types).
        $connection->setSimplifiedPsp(true);

        // Supply an API call with debug information. We should probably move this to a better place.
        $connection->setUserAgent($this->getUserAgent());

        // Deactivate auto debitable types.
        $connection->setAutoDebitableTypes(false);

        return $connection;
    }

    /**
     * @param Order $order
     *
     * @return stdClass|null
     *
     * @throws InvalidDataException
     * @throws ShopException
     * @throws ResursException
     * @throws ExceptionHandler
     */
    public function getPayment(
        Order $order
    ): ?stdClass {
        $payment = null;

        try {
            if ((string) $order->reference === '') {
                throw new InvalidDataException('No order reference.');
            }

            $payment = $this->getConnection(
                $this->getCredentialsFromOrder($order)
            )->getPayment($order->reference);

            if (!($payment instanceof stdClass)) {
                throw new InvalidDataException('Unexpected API response.');
            }
        } catch (Exception $e) {
            // If there is no payment we will receive an Exception from ECom.
            if (!$this->validateMissingPaymentException($e)) {
                throw $e;
            }
        }

        return $payment;
    }

    /**
     * Retrieve API credentials based on order data.
     *
     * @param Order $order
     *
     * @return Credentials
     *
     * @throws InvalidDataException
     * @throws ShopException
     */
    public function getCredentialsFromOrder(Order $order): Credentials
    {
        if ((int) $order->id === 0) {
            throw new InvalidDataException('Empty order object supplied.');
        }

        return $this->credentials->get(
            ShopConstraint::shop((int) $order->id_shop),
            $this->orderService->getOrder($order->id)->getTest() ?
                ResursBank::ENVIRONMENT_TEST :
                ResursBank::ENVIRONMENT_PRODUCTION
        );
    }

    /**
     * Validate that an Exception was thrown because a payment was actually
     * missing.
     *
     * @param Exception $error
     *
     * @return bool
     */
    public function validateMissingPaymentException(
        Exception $error
    ): bool {
        return
            $error->getCode() === 3 ||
            $error->getCode() === 8
        ;
    }

    /**
     * Makes a request to Resurs Bank's API to check if a payment exists for
     * an order at Resurs Bank.
     *
     * If you're planning on using the payment afterwards it's better to use
     *
     * @see getPayment and check the return value. That way you won't waste
     * time making an extra request to the API.
     *
     * @param Order $order
     *
     * @return bool
     *
     * @throws ExceptionHandler
     * @throws InvalidDataException
     * @throws ResursException
     * @throws ShopException
     */
    public function paymentExists(
        Order $order
    ): bool {
        return $this->getPayment($order) !== null;
    }

    /**
     * Creates payment model data from a generic object. Expects the generic
     * object to have the same properties as payment data fetched from the API,
     * but it's not required to. Missing properties will be created using
     * default values.
     *
     * @param bool|null $isCompany
     * @param stdClass $payment
     *
     * @return PaymentModel
     */
    public function toPayment(
        stdClass $payment,
        bool $isCompany = null
    ): PaymentModel {
        $paymentId = '';

        if (property_exists($payment, 'paymentId')) {
            $paymentId = (string) $payment->paymentId;
        } elseif (property_exists($payment, 'id')) {
            $paymentId = (string) $payment->id;
        }

        return new PaymentModel(
            $paymentId,
            property_exists(
                $payment,
                'bookPaymentStatus'
            ) ?
                (string) $payment->bookPaymentStatus :
                '',
            property_exists($payment, 'approvedAmount') ?
                (float) $payment->approvedAmount :
                0.0,
            property_exists($payment, 'signingUrl') ?
                (string) $payment->signingUrl :
                '',
            (
                property_exists($payment, 'customer') &&
                $payment->customer instanceof stdClass
            ) ?
                $this->toCustomer(
                    $payment->customer,
                    $isCompany
                ) :
                new Customer()
        );
    }

    /**
     * Creates customer model data from a generic object. Expects the generic
     * object to have the same properties as customer data fetched from the API,
     * but it's not required to. Missing properties will be created using
     * default values.
     *
     * @param bool|null $isCompany
     * @param stdClass $customer
     *
     * @return Customer
     */
    public function toCustomer(
        stdClass $customer,
        bool $isCompany = null
    ): Customer {
        $phone = property_exists($customer, 'phone') ?
            (string) $customer->phone :
            '';

        return new Customer(
            property_exists($customer, 'governmentId') ?
                (string) $customer->governmentId :
                '',
            $phone,
            property_exists($customer, 'email') ?
                (string) $customer->email :
                '',
            property_exists($customer, 'type') ?
                (string) $customer->type :
                '',
            property_exists($customer, 'address') ?
                $this->toAddress($customer->address, $isCompany, $phone) :
                null
        );
    }

    /**
     * Creates address model data from a generic object. Expects the generic
     * object to have the same properties as address data fetched from the API,
     * but it's not required to. Missing properties will be created using
     * default values.
     *
     * @param object $address
     * @param bool|null $isCompany
     * @param string $telephone
     *
     * @return Address
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function toAddress(
        object $address,
        bool $isCompany = null,
        string $telephone = ''
    ): Address {
        return new Address(
            (
                $isCompany === null &&
                property_exists($address, 'fullName') &&
                (string) $address->fullName !== ''
            ) || $isCompany,
            property_exists($address, 'fullName') ?
                (string) $address->fullName :
                '',
            property_exists($address, 'firstName') ?
                (string) $address->firstName :
                '',
            property_exists($address, 'lastName') ?
                (string) $address->lastName :
                '',
            property_exists($address, 'addressRow1') ?
                (string) $address->addressRow1 :
                '',
            property_exists($address, 'addressRow2') ?
                (string) $address->addressRow2 :
                '',
            property_exists($address, 'postalArea') ?
                (string) $address->postalArea :
                '',
            property_exists($address, 'postalCode') ?
                (string) $address->postalCode :
                '',
            property_exists($address, 'country') ?
                (string) $address->country :
                '',
            $telephone
        );
    }

    /**
     * @return string
     */
    public function getUserAgent(): string
    {
        /** @var ModuleInterface $module */
        /** @phpstan-ignore-next-line */
        $module = Module::getInstanceByName('psrbcore');

        return sprintf(
            'Prestashop | %s %s',
            $module->getName(),
            $module->getVersion()
        );
    }
}
