<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Core\Config;

use Address;
use Context;
use Country;
use Defuse\Crypto\Exception\CryptoException;
use Exception;
use InvalidArgumentException;
use PrestaShop\PrestaShop\Adapter\Configuration as Adapter;
use PrestaShop\PrestaShop\Core\Domain\Shop\ValueObject\ShopConstraint;
use Resursbank\Core\Crypto\Openssl;
use Resursbank\Core\Entity\ResursbankPaymentMethod;
use Resursbank\Core\Model\Api\Credentials;
use Resursbank\Core\Traits\Config as ConfigTrait;
use Resursbank\Ecommerce\Service\Translation;
use Resursbank\RBEcomPHP\ResursBank;
use Shop;

/**
 * Interact with the configuration table. This class provide methods to read and
 * write all values related to our module.
 */
class Config
{
    use ConfigTrait;

    /**
     * @var Adapter
     */
    private $adapter;

    /**
     * @var Openssl
     */
    private $openssl;

    /**
     * @var Context
     */
    private $context;

    /**
     * @var \psrbcore
     */
    private $module;

    /**
     * @param Adapter $adapter
     * @param Openssl $openssl
     */
    public function __construct(
        Adapter $adapter,
        Openssl $openssl
    ) {
        $this->context = Context::getContext();
        $this->adapter = $adapter;
        $this->openssl = $openssl;
        $this->module = \Module::getInstanceByName('psrbcore');
    }

    /**
     * @param ShopConstraint|null $shopConstraint
     *
     * @return string
     */
    public function getFlow(
        ShopConstraint $shopConstraint = null
    ): string {
        return (string)$this->adapter->get(
            $this->getId('flow'),
            null,
            $shopConstraint
        );
    }

    /**
     * @param string $value
     * @param ShopConstraint|null $shopConstraint
     *
     * @throws Exception
     *
     * @todo This needs to validate against a list collected throw an event dispatched before setting (add a validate method that dispatches an event, which RCO / Simplified listens to and adds their respective flow to).
     */
    public function setFlow(
        string $value,
        ShopConstraint $shopConstraint = null
    ): void {
        $this->adapter->set($this->getId('flow'), $value, $shopConstraint);
    }

    /**
     * @param ShopConstraint|null $shopConstraint
     *
     * @return int
     */
    public function getEnvironment(
        ShopConstraint $shopConstraint = null
    ): int {
        return (int)$this->adapter->get(
            $this->getId('environment'),
            ResursBank::ENVIRONMENT_TEST,
            $shopConstraint
        );
    }

    /**
     * @param int $value
     * @param ShopConstraint|null $shopConstraint
     *
     * @throws Exception
     */
    public function setEnvironment(
        int $value,
        ShopConstraint $shopConstraint = null
    ): void {
        $this->validateEnvironment($value);

        $this->adapter->set(
            $this->getId('environment'),
            $value,
            $shopConstraint
        );
    }

    /**
     * @param int $environment
     * @param ShopConstraint|null $shopConstraint
     *
     * @return string
     *
     * @throws InvalidArgumentException
     */
    public function getUsername(
        int $environment,
        ShopConstraint $shopConstraint = null
    ): string {
        $this->validateEnvironment($environment);
        return (string)$this->adapter->get(
            $this->getId("username_{$environment}"),
            null,
            $shopConstraint
        );
    }

    /**
     * @param string $value
     * @param int $environment
     * @param ShopConstraint|null $shopConstraint
     *
     * @throws Exception
     */
    public function setUsername(
        string $value,
        int $environment,
        ShopConstraint $shopConstraint = null
    ): void {
        $this->validateEnvironment($environment);

        $this->adapter->set(
            $this->getId("username_{$environment}"),
            $value,
            $shopConstraint
        );
    }

    /**
     * @param int $environment
     * @param ShopConstraint|null $shopConstraint
     *
     * @return string
     */
    public function getPassword(
        int $environment,
        ShopConstraint $shopConstraint = null
    ): string {
        $this->validateEnvironment($environment);

        $envPassword = (string)$this->adapter->get(
            $this->getId("password_{$environment}"),
            null,
            $shopConstraint
        );

        try {
            $result = $this->openssl->decrypt($envPassword);
        } catch (CryptoException $e) {
            // The below notes is still important to remember.
            //
            // @todo If your secret key changes you cannot decrypt the value.
            // @todo If an Exception is thrown the Configuration page won't render.
            // @todo We cannot implement the logger since it requires this class (circular dependency).
            //
            // The password fields are always empty, for security reasons, even when there is a value.
            // So there is no simple way to notify the client something went wrong in this scenario.
            // Not sure how to handle this yet.
            $result = $envPassword;
        }

        return $result;
    }

    /**
     * @param string $value
     * @param int $environment
     * @param ShopConstraint|null $shopConstraint
     *
     * @throws Exception
     */
    public function setPassword(
        string $value,
        int $environment,
        ShopConstraint $shopConstraint = null
    ): void {
        $this->validateEnvironment($environment);

        try {
            $passwordValue = $this->openssl->encrypt($value);
        } catch (Exception $e) {
            // Fail over if Symfony and OpenSSL fails. Note: We still can't log this.
            $passwordValue = $value;
        }

        $this->adapter->set(
            $this->getId("password_{$environment}"),
            $passwordValue,
            $shopConstraint
        );
    }

    /**
     * @param ShopConstraint|null $shopConstraint
     *
     * @return bool
     */
    public function isDebugEnabled(
        ShopConstraint $shopConstraint = null
    ): bool {
        return (bool)$this->adapter->get(
            $this->getId('debug'),
            $shopConstraint
        );
    }

    /**
     * @param bool $value
     * @param ShopConstraint|null $shopConstraint
     *
     * @throws Exception
     */
    public function setIsDebugEnabled(
        bool $value,
        ShopConstraint $shopConstraint = null
    ): void {
        // NOTE: Regarding $value ternary, PrestaShop evaluates false as NULL.
        $this->adapter->set(
            $this->getId('debug'),
            ($value === true ? 1 : 0),
            $shopConstraint
        );
    }

    /**
     * @param ShopConstraint|null $shopConstraint
     *
     * @return string
     *
     * @todo We still don't use any shopConstraint at this point, but probably should when we work with multistores.
     */
    public function getCountry(
        ShopConstraint $shopConstraint = null
    ): string {
        // Initially, we always returned 'se' here. For now on we return the shop address country instead.
        // As we returned this data in lowercase, at this moment we will continue doing that.
        return strtolower($this->getShopAddressCountry());
    }

    /**
     * @param ResursbankPaymentMethod $method
     * @return string
     * @throws Exception
     * @since 1.0.1
     */
    public function getReadMoreTranslatedString(ResursbankPaymentMethod $method): string
    {
        $translation = new Translation();
        $translation->setLanguage($this->getCountry());

        $return = $this->module->l('Read More');

        $getPhraseFrom = ['moreInfo', 'linkMore', 'extendedOpen', 'extendedLinkText'];
        foreach ($getPhraseFrom as $key) {
            $getString = $translation->getPhraseByMethod($method->getSpecificType(), $key);
            if (!empty($getString) && is_string($getString)) {
                $return = $getString;
                break;
            }
        }

        return $return;
    }

    /**
     * Get ISO based country from current shop.
     *
     * This data is retrieved from Improve => International => Localization => Default Country.
     * What we currently do not know, is if this covers a multi store setup where each store has
     * its own default country.
     *
     * @return string
     *
     * @since 1.0.0
     */
    public function getShopAddressCountry(): string
    {
        // Worst case scenario will return nothing.
        $return = '';
        if ($this->context->shop instanceof Shop) {
            $shopAddress = $this->context->shop->getAddress();
            // For some reason this instance must point at \Address if it want to be found.
            if ($shopAddress instanceof Address) {
                $return = Country::getIsoById($shopAddress->id_country);
            }
        }

        return $return;
    }


    /**
     * Resolve API credentials object for settings applied in specified shop.
     *
     * @param int $environment
     * @param ShopConstraint|null $shopConstraint
     *
     * @return Credentials
     */
    public function getCredentials(
        int $environment,
        ShopConstraint $shopConstraint = null
    ): Credentials {
        return new Credentials(
            $this->getEnvironment($shopConstraint),
            $this->getUsername($environment, $shopConstraint),
            $this->getPassword($environment, $shopConstraint),
            $this->getCountry($shopConstraint)
        );
    }

    /**
     * @param int $env
     * @param ShopConstraint|null $shopConstraint
     * @return bool
     */
    public function hasCredentials(
        int $env,
        ShopConstraint $shopConstraint = null
    ): bool {
        return (
            $this->getUsername($env, $shopConstraint) !== '' &&
            $this->getPassword($env, $shopConstraint) !== ''
        );
    }
}
