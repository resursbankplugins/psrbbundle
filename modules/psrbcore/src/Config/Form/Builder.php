<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Core\Config\Form;

use Exception;
use PrestaShop\PrestaShop\Core\Form\FormDataProviderInterface;
use PrestaShopBundle\Service\Routing\Router;
use Resursbank\Core\Config\Config;
use Resursbank\Core\Config\Form\Builder\Api;
use Resursbank\Core\Config\Form\Builder\Advanced;
use Resursbank\Core\Logger\LoggerInterface;
use Resursbank\RBEcomPHP\ResursBank;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;

/**
 * Configuration form builder.
 */
class Builder
{
    /**
     * @var FormFactoryInterface
     */
    private $formFactory;

    /**
     * @var FormDataProviderInterface
     */
    private $formDataProvider;

    /**
     * @var Router
     */
    private $router;

    /**
     * @var Config
     */
    private $config;
    /**
     * @var LoggerInterface
     *
     */
    private $logger;

    /**
     * @param FormFactoryInterface $formFactory
     * @param FormDataProviderInterface $formDataProvider
     * @param Router $router
     * @param Config $config
     * @param LoggerInterface $logger
     */
    public function __construct(
        FormFactoryInterface $formFactory,
        FormDataProviderInterface $formDataProvider,
        Router $router,
        Config $config,
        LoggerInterface $logger
    ) {
        $this->formFactory = $formFactory;
        $this->formDataProvider = $formDataProvider;
        $this->router = $router;
        $this->config = $config;
        $this->logger = $logger;
    }

    /**
     * @return FormInterface
     */
    public function getForm(): FormInterface
    {
        return $this->formFactory->createBuilder()
            ->setData($this->formDataProvider->getData())
            ->add('api', Api::class)
            ->add('advanced', Advanced::class)
            ->setAction(
                $this->router->generate('resursbank_admin_config_save')
            )->getForm();
    }



    /**
     * Validate API credentials and add an error message if a set of credentials
     * are inaccurate.
     *
     * @param int $environment
     */
    public function validateCredentials(
        int $environment
    ): void {
        $credentials = $this->config->getCredentials($environment);

        if ($credentials->getUsername() !== '') {
            $api = new ResursBank();

            try {
                $test = $api->validateCredentials(
                    $credentials->getEnvironment(),
                    $credentials->getUsername(),
                    $credentials->getPassword()
                );
            } catch (Exception $e) {
                $this->logger->exception($e);
            }
        }
    }
}
