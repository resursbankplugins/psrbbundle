<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Core\Config\Form\Builder;

use PrestaShopBundle\Form\Admin\Type\SwitchType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Generates the form containing advanced settings on config page.
 */
class Advanced extends AbstractType
{
    /**
     * {@inheritdoc}
     *
     * @noinspection PhpMissingParentCallCommonInspection
     */
    public function buildForm(
        FormBuilderInterface $builder,
        array $options
    ): void {
        $builder
            ->add('debug', SwitchType::class, [
                'required' => false,
                'label' => 'Debug',
                'help' => 'Log entries will be stored in ' .
                    'var/log/resursbank_*.log (different log files are used ' .
                    'for various operations).',
            ]);
    }

    /**
     * {@inheritdoc}
     *
     * @noinspection PhpMissingParentCallCommonInspection
     */
    public function getBlockPrefix(): string
    {
        return 'resursbank_core_advanced';
    }
}
