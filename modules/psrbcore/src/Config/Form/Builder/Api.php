<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Core\Config\Form\Builder;

use Resursbank\RBEcomPHP\ResursBank;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Generates the form containing API settings on config page.
 */
class Api extends AbstractType
{
    /**
     * {@inheritdoc}
     *
     * @noinspection PhpMissingParentCallCommonInspection
     */
    public function buildForm(
        FormBuilderInterface $builder,
        array $options
    ): void {
        $test = ResursBank::ENVIRONMENT_TEST;
        $prod = ResursBank::ENVIRONMENT_PRODUCTION;

        $builder
            ->add('flow', ChoiceType::class, [
                'label' => 'Checkout type',
                'choices' => $this->getFlowOptions(),
                'required' => false,
            ])
            ->add('environment', ChoiceType::class, [
                'label' => 'Environment',
                'choices' => [
                    'Test' => $test,
                    'Production' => $prod,
                ],
                'required' => false,
            ])
            ->add("username_{$test}", TextType::class, [
                'label' => 'Username',
                'required' => false,
            ])
            ->add("password_{$test}", PasswordType::class, [
                'label' => 'Password',
                'required' => false,
            ])
            ->add("username_{$prod}", TextType::class, [
                'label' => 'Username (production)',
                'required' => false,
            ])
            ->add("password_{$prod}", PasswordType::class, [
                'label' => 'Password (production)',
                'required' => false,
            ]);
    }

    /**
     * {@inheritdoc}
     *
     * @noinspection PhpMissingParentCallCommonInspection
     */
    public function getBlockPrefix(): string
    {
        return 'resursbank_core_api';
    }

    /**
     * @return string[]
     *
     * @todo Simplified & RCO should add their own options here, using an event specified in this method to collect options.
     */
    private function getFlowOptions(): array
    {
        return [
            'Simplified' => 'simplified',
            'RCO' => 'rco',
        ];
    }
}
